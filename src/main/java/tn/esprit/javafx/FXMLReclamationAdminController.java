package tn.esprit.javafx;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import tn.esprit.Finance_It_Team_server.entities.Reclamation;
import tn.esprit.Finance_It_Team_server.services.ServiceReclamationRemote;

/**
 * FXML Controller class
 *
 * @author hp
 */
public class FXMLReclamationAdminController implements Initializable {

    @FXML
    private TableView<Reclamation> viewReclamation;
    @FXML
    private Button btnShowReclamation;
    @FXML
    private Button btnShowAllReclamation;
    @FXML
    private TableColumn<?, ?> reclamation;
    @FXML
    private TableColumn<?, ?> status;

 
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	try {
			RemplirTable();
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
    }    


    @FXML
    private void btnShowReclamation(ActionEvent event) throws NamingException, SQLException {
    	getClicked();
    	
    	Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Description");
        alert.setHeaderText("");
        alert.setContentText(showReclamation);
        alert.showAndWait();
        String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceReclamation!tn.esprit.Finance_It_Team_server.services.ServiceReclamationRemote";
		Context context=new InitialContext();
		ServiceReclamationRemote  proxy=(ServiceReclamationRemote) context.lookup(jndiName);
		proxy.modifyState(idReclamation);
		RemplirTable();
    }

    @FXML
    private void btnShowAllReclamation(ActionEvent event) throws SQLException, NamingException {
    	RemplirTable();
    }
public void RemplirTable() throws SQLException, NamingException {
        
    	ObservableList<Reclamation> data1 = FXCollections.observableArrayList();

    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceReclamation!tn.esprit.Finance_It_Team_server.services.ServiceReclamationRemote";
		Context context=new InitialContext();
		ServiceReclamationRemote  proxy=(ServiceReclamationRemote) context.lookup(jndiName);
		reclamation.setCellValueFactory(new PropertyValueFactory<>("description"));
		status.setCellValueFactory(new PropertyValueFactory<>("state"));
		
		List<Reclamation> o = proxy.listerReclamations();
		for (Reclamation e : o) {
			data1.add(e);
		}
		viewReclamation.setItems(data1);
	}

	public static String showReclamation="";
	public static int idReclamation=0;

	    public void getClicked() {
	   	showReclamation=String.valueOf(viewReclamation.getSelectionModel().getSelectedItem().getDescription());
	   	idReclamation=viewReclamation.getSelectionModel().getSelectedItem().getId();
	   	

		}
}
