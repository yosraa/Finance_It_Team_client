package tn.esprit.javafx;


import java.net.URL;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import tn.esprit.Finance_It_Team_server.entities.User;
import tn.esprit.Finance_It_Team_server.services.ServiceUserRemote;

/**
 * FXML Controller class
 *
 * @author hp
 */
public class FXMLTraderController implements Initializable {

    @FXML
    private TextField tf_userName;
    @FXML
    private TextField tf_firstName;
    @FXML
    private TextField tf_lastName;
    @FXML
    private TextField tf_email;
    @FXML
    private TextField tf_phoneNumber;
    @FXML
    private Button btn_modifier;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
			refreshUser();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }    

    @FXML
    private void btn_modifier(ActionEvent event) throws NamingException {
    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
		Context context=new InitialContext();
		ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
		User user=proxy.getUser(FXMLLoginController.userId);
		user.setEmail(tf_email.getText());
		user.setFirstName(tf_firstName.getText());
		user.setLastName(tf_lastName.getText());
		user.setPhoneNumber(Integer.valueOf(tf_phoneNumber.getText()));
		user.setUserName(tf_userName.getText());
		proxy.modifyUser2(user, FXMLLoginController.userId);
		
    }
    private void refreshUser() throws NamingException{
    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
		Context context=new InitialContext();
		ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
		User user=proxy.getUser(FXMLLoginController.userId);
		tf_email.setText(user.getEmail());;
		tf_firstName.setText(user.getFirstName());
		tf_lastName.setText(user.getLastName());
		tf_phoneNumber.setText(String.valueOf(user.getPhoneNumber()));
		tf_userName.setText(user.getUserName());
    }
}
