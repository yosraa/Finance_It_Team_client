package tn.esprit.javafx;


import java.net.URL;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import tn.esprit.Finance_It_Team_server.services.ServiceUserRemote;

/**
 * FXML Controller class
 *
 * @author hp
 */
public class FXMLForgotPasswordController implements Initializable {

    @FXML
    private TextField tf_email;
    @FXML
    private Button btn_sendPass;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    @FXML
    private void btnSendPassword(ActionEvent event) throws NamingException {
    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
		Context context=new InitialContext();
		ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
		proxy.sendMailWithNewPassword(tf_email.getText());
		Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("New Password");
        alert.setHeaderText("");
        alert.setContentText("new password is sent to the email if you have an account with this email");
        alert.showAndWait();
    }
}
