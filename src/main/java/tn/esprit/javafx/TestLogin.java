package tn.esprit.javafx;

import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;	

public class TestLogin extends Application {
	  @Override
	    public void start(Stage stage)  throws Exception {
	        try {
	            Parent root = FXMLLoader.load(getClass().getResource("FXMLLogin.fxml"));

	            Scene scene = new Scene(root);
	            
	            stage.initStyle(StageStyle.TRANSPARENT);
	            stage.initStyle(StageStyle.UNDECORATED);
	            
	       

	            stage.setScene(scene);
	            stage.show();
	        } catch (IOException ex) {
	            Logger.getLogger(TestLogin.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
		    public static void main(String[] args) {
		        launch(args);
		    }

	}


