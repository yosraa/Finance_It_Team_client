package tn.esprit.javafx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


import tn.esprit.Finance_It_Team_server.services.*;
import tn.esprit.Finance_It_Team_server.entities.*;

/**
 * FXML Controller class
 *
 * @author hp
 */
public class FXMLLoginController implements Initializable {

    @FXML
    private PasswordField tf_Password;
    @FXML
    private TextField tf_UserName;
    @FXML
    private Button btn_Login;
    @FXML
    private Button btn_SignUp;
    @FXML
    private Button btn_ForgotPass;
    
    

    public static int userId;
   
    public static int userRole;//if(role==1) then Trader , if(role==2) then Admin

    

  
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void btn_Login(ActionEvent event) throws NamingException, IOException {
    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
		Context context=new InitialContext();
		ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
		
		List<Integer> idAndRole=proxy.login(tf_UserName.getText(), tf_Password.getText());
		System.out.println(idAndRole.get(0));
		FXMLLoginController.userId=idAndRole.get(0);
		System.out.println(idAndRole.get(1));
		FXMLLoginController.userRole=idAndRole.get(1);
		
		if(FXMLLoginController.userId==-1){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Login refused");
            alert.setHeaderText("Look, it may be one of the problems");
            alert.setContentText("username or password does not exist or your account is not valided yet by the admin");
            alert.showAndWait();
		}
		else{
			Parent root;
			if(FXMLLoginController.userRole==1)
			{
				// root = FXMLLoader.load(getClass().getResource("FXMLReclamationTrader.fxml"));
				 root = FXMLLoader.load(getClass().getResource("Client_interface_new.fxml"));

			}
			else {
				 root = FXMLLoader.load(getClass().getResource("Admin_interface_new.fxml"));
			}
			Scene newScene= new Scene(root);
		    Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
		    window.setScene(newScene);
		    window.show();
		}
		
		
    	
    }
    @FXML
    private void btn_SignUp(ActionEvent event) throws IOException {
    	Parent root=FXMLLoader.load(getClass().getResource("FXMLSignUp.fxml"));
	Scene newScene= new Scene(root);
    Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
    window.setScene(newScene);
    window.show();
    }

    @FXML
    private void btn_ForgotPass(ActionEvent event) throws IOException {
    	javafx.scene.Parent root = FXMLLoader.load(getClass().getResource("FXMLForgotPassword.fxml"));
		Stage stage = new Stage();
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }
    


    
}
