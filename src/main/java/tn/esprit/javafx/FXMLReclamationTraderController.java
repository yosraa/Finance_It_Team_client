package tn.esprit.javafx;


import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import tn.esprit.Finance_It_Team_server.services.ServiceUserRemote;
import tn.esprit.Finance_It_Team_server.entities.Reclamation;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.services.ServiceReclamationRemote;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.StateOption;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.entities.User;
import tn.esprit.Finance_It_Team_server.services.IOptionProduct;
import tn.esprit.Finance_It_Team_server.services.ServiceUserRemote;
import java.awt.event.MouseEvent;

/**
 * FXML Controller class
 *
 * @author hp
 */
public class FXMLReclamationTraderController implements Initializable {

    @FXML
    private TableView<Reclamation> viewReclamation;
    @FXML
    private TextArea tf_Reclamation;
    @FXML
    private Button btnSendReclamation;
    @FXML
    private Button btnShowReclamation;
    @FXML
    private Button btnShowAllReclamation;
    @FXML
    private TableColumn<?, ?> reclamation;
    @FXML
    private TableColumn<?, ?> status;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void btnSendReclamation(ActionEvent event) throws NamingException {
    	String reclamatio="";
    	reclamatio=tf_Reclamation.getText();
    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceReclamation!tn.esprit.Finance_It_Team_server.services.ServiceReclamationRemote";
		Context context=new InitialContext();
		ServiceReclamationRemote  proxy=(ServiceReclamationRemote) context.lookup(jndiName);
		
		
		
		Reclamation reclamation=new Reclamation(0, "", reclamatio, FXMLLoginController.userId, "");
		proxy.ajouterReclamation(reclamation);
    }

    @FXML
    private void btnShowReclamation(ActionEvent event) {
    	getClicked();
    	Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Description");
        alert.setHeaderText("");
        alert.setContentText(showReclamation);
        alert.showAndWait();

    }

    @FXML
    private void btnShowAllReclamation(ActionEvent event) throws SQLException, NamingException {
    	RemplirTable();
    }
public void RemplirTable() throws SQLException, NamingException {
        
    	ObservableList<Reclamation> data1 = FXCollections.observableArrayList();

    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceReclamation!tn.esprit.Finance_It_Team_server.services.ServiceReclamationRemote";
		Context context=new InitialContext();
		ServiceReclamationRemote  proxy=(ServiceReclamationRemote) context.lookup(jndiName);
		reclamation.setCellValueFactory(new PropertyValueFactory<>("description"));
		status.setCellValueFactory(new PropertyValueFactory<>("state"));
		
		List<Reclamation> o = proxy.listerReclamationParTrader(FXMLLoginController.userId);
		for (Reclamation e : o) {
			data1.add(e);
		}
		viewReclamation.setItems(data1);
	}

	public static String showReclamation="";
	    public void getClicked() {
	   	showReclamation=String.valueOf(viewReclamation.getSelectionModel().getSelectedItem().getDescription());
			

		}

}
