package tn.esprit.javafx;


import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import tn.esprit.Finance_It_Team_server.services.ServiceUserRemote;

/**
 * FXML Controller class
 *
 * @author hp
 */
public class FXMLChartLogsController implements Initializable {

    @FXML
    private BarChart<?, ?> loginChart;
    @FXML
    private NumberAxis y;
    @FXML
    private CategoryAxis x;

    /**
     * Initializes the controller class.
     */
	@Override
    public void initialize(URL url, ResourceBundle rb) {
		try {
			graph();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
}
	public void graph() throws NamingException{
		LocalDate dd = FXMLLoginLogsController.localdd;
        LocalDate df = FXMLLoginLogsController.localdf;     
   
    		XYChart.Series set1 =new XYChart.Series<>();
            set1.setName("Login min/day");
            String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
    		Context context=new InitialContext();
    		ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
    		int i=0;
    		do {     
                
                    LocalDate qdd=dd;
                    LocalDate qdf=df;

                    set1.getData().add(new XYChart.Data(qdd.toString(),proxy.allMinutesByday(qdd, FXMLAdminController.ttId)));

                    dd=dd.plusDays(1);
                    System.out.println(qdd.toString()+"   "+proxy.allMinutesByday(qdd, FXMLAdminController.ttId));
                    
                    
    		
                    } while (dd.getYear()!=df.getYear()||dd.getDayOfYear()!=df.getDayOfYear());
      loginChart.getData().addAll(set1);
    	
    }
	
}
