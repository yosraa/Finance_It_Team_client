package tn.esprit.javafx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.services.ServiceUserRemote;

/**
 * FXML Controller class
 *
 * @author hp
 */
public class FXMLSignUpController implements Initializable {

    @FXML
    private TextField tf_UserName;
    @FXML
    private TextField tf_FirstName;
    @FXML
    private TextField tf_email;
    @FXML
    private TextField tf_Password;
    @FXML
    private TextField tf_LastName;
    @FXML
    private Button btn_SignUp;
    @FXML
    private Button btn_returnToLogin;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void btn_SignUp(ActionEvent event) throws NamingException {
    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
		Context context=new InitialContext();
		ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
		Trader trader=new Trader(tf_UserName.getText(), tf_FirstName.getText(), tf_LastName.getText(), tf_email.getText(), tf_Password.getText(), "M", 1234556, "0", "Pending");
		if(traderControl(trader).equals("ok")){
		proxy.addUser(trader);
		}
		else{
			Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("SignUp refused");
            alert.setHeaderText("");
            alert.setContentText(traderControl(trader));
            alert.showAndWait();
		}
    }

    public String checkString(String string){

Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
Matcher m = p.matcher(string);
boolean b = m.find();
if (b)return("no");
return("ok");
    }
    public String checkEmail(String email){

    	Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
    	Matcher m = p.matcher(email);
    	boolean matchFound = m.matches();
    	if (matchFound) {
    		return "ok" ;   	
    	    }
    	    return "no";
    }
    public String traderControl(Trader trader) throws NamingException{
    	if(trader.getUserName().isEmpty())return"username empty";
    	if(trader.getFirstName().isEmpty())return"first name empty";
    	if(trader.getLastName().isEmpty())return"last name empty";
    	if(trader.getPassword().isEmpty())return"password empty";
    	if(trader.getEmail().isEmpty())return"email empty";
    	if(checkString(trader.getUserName()).equals("no"))return"no special characters in username";
    	if(checkString(trader.getFirstName()).equals("no"))return"no special characters in First name";
    	if(checkString(trader.getLastName()).equals("no"))return"no special characters in Last name";
    	if(checkString(trader.getPassword()).equals("no"))return"no special characters in password";
    	if(checkEmail(trader.getEmail()).equals("no"))return"email is not valid";
    	
    	
    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
		Context context=new InitialContext();
		ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
		if(proxy.emailExists(trader.getEmail()))return"email already exists";
		if(proxy.usernameExists(trader.getUserName()))return"username already exists";

		return "ok";
    	
    	
    }
    
    @FXML
    private void btn_returnToLogin(ActionEvent event) throws IOException {
    	Parent root=FXMLLoader.load(getClass().getResource("FXMLLogin.fxml"));
	Scene newScene= new Scene(root);
    Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
    window.setScene(newScene);
    window.show();
    }
    
}
