package tn.esprit.javafx;
	
import java.io.IOException;
import java.util.logging.Logger;

import org.hibernate.internal.StaticFilterAliasGenerator;
import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;

import java.util.logging.Level;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class Main extends Application {
	@Override
	  public void start(Stage stage) throws Exception {
        try { 
             Parent root = FXMLLoader.load(getClass().getResource("AdminTrading1.fxml"));
            Scene scene = new Scene(root);
            
            stage.initStyle(StageStyle.TRANSPARENT);
             
            stage.setScene(scene);
            stage.show();
            
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
        currencyConvertion("USD", "EUR");
        
    }
    public static void currencyConvertion(String from,String to)
	{
		String response = HttpRequest
				.get("https://v3.exchangerate-api.com/bulk/428d417084fe51418dc991a4/"+from)
				.accept("application/json").body();
		JSONObject jsonObject = new JSONObject(response);
		//
		JSONObject status = jsonObject.getJSONObject("rates");
		Double eur = status.getDouble(to);
		
	}   
}
