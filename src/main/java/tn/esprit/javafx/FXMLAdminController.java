package tn.esprit.javafx;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.StateOption;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.entities.User;
import tn.esprit.Finance_It_Team_server.services.IOptionProduct;
import tn.esprit.Finance_It_Team_server.services.ServiceUserRemote;
import java.awt.event.MouseEvent;
import java.io.IOException;

/**
 * FXML Controller class
 *
 * @author hp
 */
public class FXMLAdminController implements Initializable {

    @FXML
    private TextField tf_Id;
    @FXML
    private Button btn_delete;
    @FXML
    private Button btn_accept;
    @FXML
    private Button btn_ban;
    @FXML
    private Button btn_afficher;
    @FXML
    private TableView<Trader> viewTraders;
    @FXML
    private TableColumn<?, ?> id;
    @FXML
    private TableColumn<?, ?> username;
    @FXML
    private TableColumn<?, ?> firstName;
    @FXML
    private TableColumn<?, ?> lastName;
    @FXML
    private TableColumn<?, ?> email;
    @FXML
    private TableColumn<?, ?> status;
    @FXML
    private Button btnLogs;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	tf_Id.setText("0000");
    	try {
			RemplirTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }    
    
    @FXML
    private void btn_delete(ActionEvent event) throws NamingException {
    	getClicked();
    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
		Context context=new InitialContext();
		ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
		Trader trader=new Trader();
		
		proxy.deleteUser(trader,Integer.valueOf(tf_Id.getText()));
		
    }

    @FXML
    private void btn_accept(ActionEvent event) throws NamingException {
    	getClicked();
    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
		Context context=new InitialContext();
		ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
		Trader trader=new Trader();
		trader.setStatusTrader("Accepted");
		proxy.modifyUser(trader,Integer.valueOf(tf_Id.getText()));
    }

    @FXML
    private void btn_ban(ActionEvent event) throws NamingException {
    	getClicked();
    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
		Context context=new InitialContext();
		ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
		Trader trader=new Trader();
		trader.setStatusTrader("Banned");

		proxy.modifyUser(trader,Integer.valueOf(tf_Id.getText()));
    }

    @FXML
    private void btn_afficher(ActionEvent event) throws SQLException, NamingException {
    	RemplirTable();
    }
    public void RemplirTable() throws SQLException, NamingException {
        
    	ObservableList<Trader> data1 = FXCollections.observableArrayList();

    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
		Context context=new InitialContext();
		ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
		id.setCellValueFactory(new PropertyValueFactory<>("id"));
		username.setCellValueFactory(new PropertyValueFactory<>("userName"));
		firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
		lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
		email.setCellValueFactory(new PropertyValueFactory<>("email"));
		status.setCellValueFactory(new PropertyValueFactory<>("statusTrader"));
		
		List<Trader> o = proxy.getAllTraders();
		for (Trader e : o) {
			data1.add(e);
		}
		viewTraders.setItems(data1);
	}

    public static int ttId;
	    public void getClicked() {
	   	tf_Id.setText(String.valueOf(viewTraders.getSelectionModel().getSelectedItem().getId()));
		ttId=viewTraders.getSelectionModel().getSelectedItem().getId();

		}
	    @FXML
	    private void btnLogs(ActionEvent event) throws IOException {
	    	getClicked();
	    	javafx.scene.Parent root = FXMLLoader.load(getClass().getResource("FXMLLoginLogs.fxml"));
			Stage stage = new Stage();
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
	    }
    
}
