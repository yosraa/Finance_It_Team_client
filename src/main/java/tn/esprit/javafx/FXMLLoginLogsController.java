package tn.esprit.javafx;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import tn.esprit.Finance_It_Team_server.entities.LoginLogs;
import tn.esprit.Finance_It_Team_server.entities.Reclamation;
import tn.esprit.Finance_It_Team_server.services.ServiceReclamationRemote;
import tn.esprit.Finance_It_Team_server.services.ServiceUserRemote;


public class FXMLLoginLogsController implements Initializable {

    @FXML
    private Button btnGraph;
    @FXML
    private DatePicker dateDebut;
    @FXML
    private DatePicker dateFin;
    @FXML
    private TableView<LoginLogs> viewLogs;
    @FXML
    private TableColumn<?, ?> dateLogin;
    @FXML
    private TableColumn<?, ?> dateLogout;
    @FXML
    private TableColumn<?, ?> traderId;
    @FXML
    private TableColumn<?, ?> timeSpent;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	try {
			RemplirTable();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }    

    public static LocalDate localdd;
    public static LocalDate localdf;
    @FXML
    private void btnGraph(ActionEvent event) throws IOException {
    	localdd=dateDebut.getValue();
    	localdf=dateFin.getValue();    	
    	javafx.scene.Parent root = FXMLLoader.load(getClass().getResource("FXMLChartLogs.fxml"));
		Stage stage = new Stage();
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
    }
public void RemplirTable() throws SQLException, NamingException {
        
    	ObservableList<LoginLogs> data1 = FXCollections.observableArrayList();

    	String jndiName="Finance_It_Team_server-ear/Finance_It_Team_server-ejb/ServiceUser!tn.esprit.Finance_It_Team_server.services.ServiceUserRemote";
		Context context=new InitialContext();
		ServiceUserRemote  proxy=(ServiceUserRemote) context.lookup(jndiName);
		dateLogin.setCellValueFactory(new PropertyValueFactory<>("date"));
		dateLogout.setCellValueFactory(new PropertyValueFactory<>("dateFin"));
		traderId.setCellValueFactory(new PropertyValueFactory<>("traderId"));
		timeSpent.setCellValueFactory(new PropertyValueFactory<>("minutes"));
		List<LoginLogs> o = proxy.listLoginLogsOfTrader(FXMLAdminController.ttId);
		for (LoginLogs e : o) {
			data1.add(e);
		}
		viewLogs.setItems(data1);
	}

	
}
