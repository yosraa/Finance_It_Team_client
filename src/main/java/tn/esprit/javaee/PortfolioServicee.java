package tn.esprit.javaee;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.Portfolio;
import tn.esprit.Finance_It_Team_server.entities.StateOption;
import tn.esprit.Finance_It_Team_server.entities.TypeOption;
import tn.esprit.Finance_It_Team_server.services.IPortfolioService;
import tn.esprit.Finance_It_Team_server.services.PortfolioServiceRemote;


// Add is working 
// Delete is working
// Update is working but im wondering if setId is always there or not of the update
// show by adding a static Id is working

public class PortfolioServicee {
	public static void main(String[] args) throws NamingException,InterruptedException
	{

		
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/PortfolioServiceRemote!tn.esprit.Finance_It_Team_server.services.IPortfolioService";
		Context ctx = new InitialContext();
		IPortfolioService proxy = (IPortfolioService) ctx.lookup(jndiName);

		//Portfolio portfolio = new Portfolio("JEMMALI"); // id=6
		//proxy.addPortfolio(portfolio);

		//Portfolio portfolio1 = new Portfolio("MOMO"); // id=7
		//proxy.addPortfolio(portfolio1);
		//System.out.println("a portfolio was added");
		
		// proxy.deletePortfolio(7);	// delete the portfolio which the id=7 MOMO will be deleted
		//System.out.println("portfolio deleted");
		
			//Portfolio portfolio2 = new Portfolio();
		// portfolio2.setId(11);
		
		Portfolio portfolio3 = new Portfolio(); 
		portfolio3.setId(13);		// you mast write a new instance which is empty and after that you should set the id that you want to update 
	portfolio3.setName("amine");
		proxy.updatePortfolio(portfolio3);
		System.out.println("portfolio updated");
		
	//	String names = proxy.showPortfolio(6);
	//	System.out.println("*************the names are: **************"+names);
		
	}


}
