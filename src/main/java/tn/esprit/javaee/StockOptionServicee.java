package tn.esprit.javaee;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.Finance_It_Team_server.entities.StateOptionStock;
import tn.esprit.Finance_It_Team_server.entities.Stock;
import tn.esprit.Finance_It_Team_server.entities.StockOption;
import tn.esprit.Finance_It_Team_server.entities.Trader;
import tn.esprit.Finance_It_Team_server.entities.TypeOption;
import tn.esprit.Finance_It_Team_server.entities.TypeOptionStock;
import tn.esprit.Finance_It_Team_server.services.IStockOptionService;
import tn.esprit.Finance_It_Team_server.services.StockOptionServiceRemote;
import tn.esprit.Finance_It_Team_server.entities.User;
import tn.esprit.Finance_It_Team_server.services.IStockService;

public class StockOptionServicee {

	public static void main(String[] args) throws NamingException, InterruptedException {

		// String jndiName1 =
		// "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/PortfolioServiceRemote!tn.esprit.Finance_It_Team_server.services.IPortfolioService";

		String jndiName1 = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";

		Context ctx1 = new InitialContext();
		IStockOptionService proxy1 = (IStockOptionService) ctx1.lookup(jndiName1);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		// temporalValues.setUtilTimestamp(new SimpleDateFormat("yyyy-MM-dd
		// HH:mm:ss.SSS").parse("2017-11-15 15:30:14.332"));

		Date date = new Date();
		/*
		 * Trader t = new Trader("MyriamMalek", "Jemmali",
		 * "myriammalek.jemmali@esprit.tn", "1234", "Femme", 53896583, "xxx",
		 * "activaited");
		 * x
		 * int j = proxy1.addStockOptionUser(t);
		 */ // I didn't use it

		StockOption so = new StockOption(17865.f, 5757.f, TypeOptionStock.call, date, 157.f, 54465.f, 11.f,
				StateOptionStock.yes, date, date, 400.f);

		StockOption so1 = new StockOption(17865.f, 5757.f, TypeOptionStock.put, date, 157.f, 54465.f, 11.f,
				StateOptionStock.yes, date, date, 400.f);
		System.out.println("************************* ADD StockOption ************************************");
		//proxy1.addStockOption(so1);

		System.out.println(
				"************************* ShowStockOption: return the nb of shares of an entred id ************************************");

	/*	float y = proxy1.showStockOption(1);
		System.out.println("The Nb of shares are: " + y);*/

		System.out.println("************************* Update ************************************");
		// -------update--------
		/*
		 * StockOption so11 = new StockOption(); //onze mouch double L
		 * so11.setId(3); so11.setStrikePriceStock(7887.f);
		 * so11.setCurrentPriceStock(778.f);
		 * so11.setTypeOptionStock(TypeOptionStock.put);
		 * so11.setExpirationDateStock(date); so11.setVolatilityStock(887.f);
		 * so11.setRiskStock(888.f); so11.setTimeStock(8000.f);
		 * so11.setStateOptionStock(StateOptionStock.no);
		 * so11.setDateOptionBegin(date); so11.setDateOptionEnd(date);
		 * so11.setNbShares(800.f);
		 * 
		 * proxy1.updateStockOption(so11);
		 */
		// --------------finUpdate---------

		System.out.println("************************* Delete ************************************");
		// proxy1.deleteStockOption(2);

		System.out.println(
				"************************* Affect a StockOption to a Trader ************************************");
		//proxy1.affectAStockOptionToTrader(7, 1); //is working

		System.out.println("************************* getStockOptionByNbShares************************************");
		// System.out.println("*************************Display the LIST of All
		// Nb of shares in the table
		// StockOption************************************");
		List<StockOption> soo = proxy1.getAllStockOption(); // list of all Nb of
															// shares
		System.out.println("The LIST of Nb of shares : ");
		for (StockOption s : soo) {
			System.out.println(s.getNbShares());
		}

		System.out.println(
				"*************************Display the LIST of All StockOption************************************");

		List<StockOption> soo2 = proxy1.getAllStockOption(); // list of all Nb
																// of shares
		System.out.println("The LIST of Nb of shares : ");
		for (StockOption s : soo2) {
			System.out.println(s);
		}

		System.out.println(
				"*************************Return's the Number of ROWS of the table StockOption************************************");

		List<StockOption> soo3 = proxy1.getAllStockOption(); // list of all Nb
																// of shares
		/*
		 * for(StockOption s :soo3) { //System.out.println(soo3.size());//nb of
		 * rows of StockOption table in the DataBase }
		 */
		System.out.println("Number of ROWS in the table StockOption in th DataBase : " + soo3.size());

		// Not working i've replace it with another function
		/*
		 * List<StockOption>soooo=proxy1.findAllStockOptions();
		 * System.out.println(soooo.size()); //traja3li the nb of rows fel table
		 * el kol
		 */
		// List<StockOption>sooo=proxy1.findAllStockOptions(); // list of all Nb
		// of shares
		// System.out.println("all rows");
		/*
		 * for(StockOption s :sooo) { System.out.println(s.toString()); }
		 */

		// proxy1.affectAStockToAStockOption(1,1); //temchi f classe stock mouch houni

		/*List<StockOption> soo7 =proxy1.getChecked();
		for (StockOption s : soo7) {
			s.toString();
		}*/ //not working
		
		//-----------------------CHEKED not working ------------------------
	//	  List<StockOption> ideas = new ArrayList<>();
     //     IStockOptionService Idea_test = new StockOptionServiceRemote();

     //     List<StockOption>sooo  =proxy1.findAllStockOptions();
	   //     System.out.println(Idea_test);
  /*     System.out.println(proxy1.getIdeeById(1));
       proxy1.getIdeesByUserId(1).forEach((i) -> {
	          System.out.println(i);
	           System.out.println("\n");
	        });*/

      //  Idea_test.getChecked() .forEach((i) -> {
	//            System.out.println(i);
    //    });
//	        Idea_test.getNonChecked().forEach((i) -> {
//	            System.out.println(i);
//	        });

		System.out.println("**************************Calcul Intrinsic Value**************************** ");
		System.out.println("aaaaaaa");
	
		System.out.println("Call Option Intrinsic Value = Underlying Stock's Current Price � Call Strike Price: ");
		float z = 0.0f;
		System.out.println("Call Option Intrinsic Value#0: " + z);
		 z = proxy1.StockOptionPutCall(12);
		System.out.println("Call Option Intrinsic Value: " + z);

		float z1 = proxy1.StockOptionPutCall(3);
		System.out.println("Put Option Intrinsic Value = Put Strike Price � Underlying Stock's Current Price");
		System.out.println("put Option Intrinsic Value: " + z1);

		System.out.println("**************************The Value of the Shares**************************** ");
		float u = 0.0f;
		System.out.println("The Value of the Shares#0: " + u);
	//	u = proxy1.calculValueShares(7);
	//	u = proxy1.calculValueShares(10.f,10.f,100.f);
	//	System.out.println("The Value of the Shares: " + u);

		System.out.println("**************************Time value**************************** ");

		float t = proxy1.StockOptionTimeValue(6);
		System.out.println("Time Value: " + t);

		System.out.println("**************************Futures Contract**************************** ");
		float fp = (float) proxy1.futuresPrice(6);
		System.out.println("Futures Contract: " + fp);

		System.out.println("**************************Option Price Formula (premium)**************************** ");

		float opf = proxy1.OptionPriceFormula(6);
		System.out.println("Call Price Formula (premium): " + opf);

		float opf1 = proxy1.OptionPriceFormula(3);
		System.out.println("Put Price Formula (premium): " + opf1);

		System.out.println("**************************Option's EVALUATION **************************** ");
		//String oe1 = proxy1.DeepInOutAtTheMoney(12);
		//System.out.println("CALL Option EVALUATION: " + oe1);
		
		//String oe2 = proxy1.DeepInOutAtTheMoney(31);
		//System.out.println("PUT Option EVALUATION: " + oe2);
		
		System.out.println("**************************Price of a LOOKBACK CALL OPTION**************************** ");
		float p=proxy1.PriceOfALookbackOption(10000,1000,0.4f,15,20,7);
		System.out.println("Price of a LOOKBACK CALL OPTION: " + p);
		
		System.out.println("**************************Pricing De Warrant Adjusted to Black & Schools**************************** ");

		float w=proxy1.WarrantPriceAdjustedBS(13);
		System.out.println("Pricing De Warrant Adjusted to Black & Schools: " + w);
		


		// float t=proxy1.StockOptionTimeValue(3);
		// System.out.println("Time Value: " +t);

	}
}
