package tn.esprit.javaee;


import java.lang.reflect.Proxy;
import java.util.Date;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;

import tn.esprit.Finance_It_Team_server.entities.CurrencyAccount;
import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.StateTrade;
import tn.esprit.Finance_It_Team_server.services.CalculServiceRemote;
import tn.esprit.Finance_It_Team_server.services.IOptionProduct;

public class OptionProductService {
	public static void main(String[] args) throws NamingException, InterruptedException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";
		Context ctx = new InitialContext();
		IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);
		// DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		// temporalValues.setUtilTimestamp(new SimpleDateFormat("yyyy-MM-dd
		// HH:mm:ss.SSS").parse("2017-11-15 15:30:14.332"));
		/******************* Calcul ********************/
		String jndiName2 = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/CalculService!tn.esprit.Finance_It_Team_server.services.CalculServiceRemote";

		Context context2 = new InitialContext();
		CalculServiceRemote proxy2 = (CalculServiceRemote) context2.lookup(jndiName2);
		/*****************************************/

/*		Date date = new Date();
		// CurrencyOption optionp = new
		// CurrencyOption(100.1f,200.1f,TypeOption.put,1.1f,2.1f,4,StateOption.unavailable,date);
		// Trader t = new Trader("malek","jemmali", Gender.Female,
		// date,"myriammalek.jemmali@esprit.tn",(long)53896583,"momo",Role.Admin,"aaa","available");

		// Trader t=new Trader("amine","abid","amine.abid@esprit.tn","2255",
		// "M", 22845795,"aaa", "activaited");

		// CurrencyOption optionp =new CurrencyOption(500.f, 600.f, 0.4f, 0.7f,
		// 5, TypeOption.call, StateOption.unavailable,date,
		// UnitExchange.USD_EUR,2.4f);
		// int i = proxy.addOptionProduct(optionp);
		// int j = proxy.addOptionUser(t);

		// CurrencyOption optionp =new CurrencyOption(600.f, 600.f, 0.4f, 0.7f,
		// 5, TypeOption.put, StateOption.available,date,
		// UnitExchange.JPY_EUR,2.4f);

		// CurrencyOption optionp =new CurrencyOption(500.f, 600.f, 0.4f, 0.7f,
		// 5, TypeOption.call, StateOption.unavailable,date,
		// UnitExchange.USD_EUR,2.4f);
		// int i = proxy.addOptionProduct(optionp);

		// CurrencyOption optionp =new CurrencyOption(500.f, 600.f, 0.4f, 0.7f,
		// 5, TypeOption.call, StateOption.unavailable,date,
		// UnitExchange.USD_EUR,2.4f);
		// int i = proxy.addOptionProduct(optionp);

		// int j = proxy.addOptionUser(t);

		// proxy.deleteOptionProduct(1);
		// proxy.affecterTraderOption(1,1);
		// float price = proxy.readOptionProductt(2);
		// CurrencyOption o =proxy.readOptionProduct(2);
		// System.out.println(o);
		// String a=proxy.toString();
		// System.out.println(a);
		// System.out.println("msg1 "+o.getCurrentPrice()+"msg2"+o.getRisk()+"
		// "+o.getSpotExchangeRate()+" "+o.getStrikePrice()+" "+o.getTime()+"
		// "+o.getVolatility()+" "+o.getExpirationDate()+"
		// "+o.getUnitEchange()+" "+o.getTypeoption()+" "+o.getStateOption());
		Date date1 = new Date();
		// date.setTime(3);
		// date.now();

		// CurrencyOption optionp = new
		// CurrencyOption(100.1f,200.1f,TypeOption.call,1.1f,2.1f,4,StateOption.unavailable,date,UnitExchange.EUR_TND,3.0f);

		// System.out.println("ssssss");
		// System.out.println(price);

		List<CurrencyOption> o = proxy.getAllCurrencyOption();
		for (CurrencyOption e : o) {
			System.out.println(e.getCurrentPrice());
		}

		
		 * CurrencyOption op
		 * =proxy.getCurrencyOptionByTypeAndUnitExchange(StateOption.unavailable
		 * ,UnitExchange.USD_EUR); System.out.println("tester  "+op);
		 

		
		 * CurrencyOption option1 = new CurrencyOption(); option1.setId(1);
		 * option1.setStrikePrice(500); option1.setCurrentPrice(300);
		 * option1.setTypeoption(TypeOption.call); option1.setVolatility(1.5f);
		 * option1.setTime(4); option1.setStateOption(StateOption.available);
		 * proxy.updateOption(option1);
		 

		// CurrencyOption optionp = new
		// CurrencyOption(100.1f,200.1f,TypeOption.put,1.1f,2.1f,4,StateOption.unavailable,date);
		// Trader t = new Trader("amine", "abid", "amine.abid@esprit.tn",
		// "2255", "M", 22845795, "aaa", "activaited");

		// CurrencyOption optionp = new
		// CurrencyOption(100.1f,200.1f,TypeOption.put,1.1f,2.1f,4,StateOption.unavailable,date);
		// Trader t=new Trader("amine","abid","amine.abid@esprit.tn","2255",
		// "M", 22845795,"aaa", "activaited");
		// CurrencyOption optionp =new CurrencyOption(500.f, 600.f, 0.4f, 0.7f,
		// 5, TypeOption.call, StateOption.unavailable,date,
		// UnitExchange.USD_EUR,2.4f);
		// int i = proxy.addOptionProduct(optionp);
		// int j = proxy.addOptionUser(t);
		// proxy.deleteOptionProduct(1);
		proxy.affecterTraderOption(1, 1);
		// float price = proxy.readOptionProductt(2);
		// CurrencyOption o =proxy.readOptionProduct(2);
		// System.out.println(o);
		// String a=proxy.toString();
		// System.out.println(a);
		// System.out.println("msg1 "+o.getCurrentPrice()+"msg2"+o.getRisk()+"
		// "+o.getSpotExchangeRate()+" "+o.getStrikePrice()+" "+o.getTime()+"
		// "+o.getVolatility()+" "+o.getExpirationDate()+"
		// "+o.getUnitEchange()+" "+o.getTypeoption()+" "+o.getStateOption());

		
		 * CurrencyOption optionp = new CurrencyOption(856.f, 897.f, 0.3f, 0.7f,
		 * 5, TypeOption.put, date, UnitExchange.JPY_EUR, 1.4f, 25);
		 * optionp.setStateOption(StateOption.unavailable);
		 * optionp.setArchive(0); double x = proxy2.callPrice(23.75, 15.00,
		 * 0.01, 0.35, 0.5); CurrencyOption optionp1 = new CurrencyOption(789.f,
		 * 658.f, 1.4f, 1.7f, 7, TypeOption.call, date, UnitExchange.USD_EUR,
		 * 2.9f, x); optionp1.setStateOption(StateOption.unavailable);
		 * optionp1.setArchive(1);
		 

		// int i = proxy.addOptionProduct(optionp1);
		// int k = proxy.addOptionProduct(optionp);
		// int j = proxy.addOptionUser(t);
		// proxy.deleteOptionProduct(1);

		// System.out.println("ssssss");
		// System.out.println(price);

		
		 * List<CurrencyOption>o=proxy.getAllCurrencyOption();
		 * for(CurrencyOption e :o) { System.out.println(e.getCurrentPrice()); }
		 
		
		 * CurrencyOption op
		 * =proxy.getCurrencyOptionByTypeAndUnitExchange(StateOption.unavailable
		 * ,UnitExchange.USD_EUR); System.out.println("tester  "+op);
		 

		
		 * CurrencyOption option1 = new CurrencyOption(); option1.setId(1);
		 * option1.setStrikePrice(500); option1.setCurrentPrice(300);
		 * option1.setTypeoption(TypeOption.call); option1.setVolatility(1.5f);
		 * option1.setTime(4); option1.setStateOption(StateOption.available);
		 * proxy.updateOption(option1);
		 

		// proxy.affecterTraderOption(1,1);

		// proxy.affecterOptionATrader(1,3);

		// float price = proxy.readOptionProductt(2);
		// CurrencyOption o =proxy.readOptionProduct(2);
		// System.out.println(o);
		// String a=proxy.toString();
		// System.out.println(a);
		// System.out.println("msg1 "+o.getCurrentPrice()+"msg2"+o.getRisk()+"
		// "+o.getSpotExchangeRate()+" "+o.getStrikePrice()+" "+o.getTime()+"
		// "+o.getVolatility()+" "+o.getExpirationDate()+"
		// "+o.getUnitEchange()+" "+o.getTypeoption()+" "+o.getStateOption());

		// float price = proxy.readOptionProduct(1);
		// System.out.println("price is "+price);

		// System.out.println("ssssss");
		// System.out.println(price);
		// proxy.affecterTraderOption(1,2);

		System.out.println("*****************************list all currency option******************");
		List<CurrencyOption> oc = proxy.getAllCurrencyOption();
		for (CurrencyOption e : oc) {
			System.out.println(e);
		}

		
		 * System.out.println(
		 * "***********************************type*******************************"
		 * ); List<CurrencyOption> c = proxy.getCurrencyOptionTypeCall(); for
		 * (CurrencyOption e : c) { System.out.println("vendredi " + c); }
		 
		System.out.println("************************************************************************");
		List<CurrencyOption> op1 = proxy.getCurrencyOptionById();
		for (CurrencyOption e : op1) {
			System.out.println("vendredi " + e);
		}
		System.out.println("**********************************available********************************");

		List<CurrencyOption> optiona = proxy.getAvailableEtat();
		for (CurrencyOption e : optiona) {
			System.out.println("available  " + e);
		}
		
		 * System.out.println(
		 * "**********************************unavailable********************************"
		 * ); List<CurrencyOption> optionu = proxy.getAvailableEtat(); for
		 * (CurrencyOption e : optionu) { System.out.println("available  " + e);
		 * }
		 
		
		 * List<CurrencyOption>opp=proxy.afficherCurrencyOptionByIdTrader();
		 * for(CurrencyOption e :opp) {
		 * System.out.println("affichage currency option of trader "+e); }
		 

		// CurrencyOption op
		// =proxy.getCurrencyOptionByTypeAndUnitExchange(600.0f,600.0f);
		// System.out.println("tester "+op.getStateOption());

		// CurrencyOption optionp = new CurrencyOption();
		
		 * System.out.
		 * println("*********************change the state of option unvailable to available*************"
		 * ); optionp.setId(14); optionp.setStateOption(StateOption.available);
		 * proxy.updateOption(optionp);
		 
		System.out.println(
				"***************************Fonction Metier*******************************************************");

		float gain = proxy.gainOrLossOfTraderCallAndPut(21);
		System.out.println("gain or loss " + gain);
		System.out.println("************************************************************************************");

		
		 * Long typeoption=proxy.nbCurrencyOption();
		 * System.out.println("testeer nombre "+typeoption);
		 
		System.out.println(
				"*****************************Nombre by Unit Exchange******************************************************");
		// List<CurrencyOption>opp=proxy.getCurrencyOptionOfTrader(1);
		// for(CurrencyOption e :opp) {
		// System.out.println("affichage currency option of trader "+e); }

		// List<Long> m = proxy.nbCurrencyOption2();

		
		 * for (Long h : m) { System.out.println("nombre " + h); }
		 * proxy.affecterTraderOption(1,51);
		 * 
		 * // System.out.println(proxy.findOptionByType(600.0f));/*
		 * //proxy.affecterTraderOption(1,51); // proxy.deleteOptionUser(1);
		 * //proxy.deleteOptionUser(2);
		 * 
		 * 
		 * System.out.
		 * println("*********************************different de id trader ************************************************"
		 * ); /*
		 * List<CurrencyOption>opp=proxy.getAllCurrencyOptionNotIdTrader(int t);
		 * for(CurrencyOption e :opp) {
		 * System.out.println("affichage currency option  "+e); }
		 

		System.out.println("***********************************************************************************");
		
		 * List<Date>datee=proxy.StrikePricePermonth(); for(Date e : datee) {
		 * System.out.println("date "+e); }
		 * 
		 * List<Float>price=proxy.StrikePriceOfThisMonth(); for(Float e : price)
		 * { System.out.println("price "+e); }
		 * 
		 * List<Object[]>l=proxy.StrikePriceOfThisMonth1(); for(Object[] e : l)
		 * { System.out.println("object  "+e); }
		 

		System.out.println("***********************************************************************************");
		//proxy.validateStateOfTrade(115, 100, 11, StateTrade.New);
*/		System.out.println("**********************amount of tarder*****************************************");
		 List<Float>opp=proxy.getAmountFromCurrencyAccount();
		 for(Float e :opp) {
		 System.out.println("affichage des amount  "+e); }
		 System.out.println("****************************************************************************");
		 long c=proxy.countRisk();
			long mc=proxy.countRiskMax();
			long mcm=proxy.countRiskMin();
			//float pourcentage =(long)((float)mc/c*100);
			System.out.println(mc);
			System.out.println(mcm);
			//System.out.println(pourcentage+"%");
			System.out.println("*****************************************************************");
			//proxy.currencyConvertion("USD","EUR");
	}
	
		


	
}
