package tn.esprit.Controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.CheckBox;
import javafx.scene.input.MouseEvent;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import static java.time.temporal.TemporalQueries.localDate;

import java.util.concurrent.TimeUnit;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;

import javafx.collections.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javafx.collections.*;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.*;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;
import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.RealData;
import tn.esprit.Finance_It_Team_server.entities.TypeOption;
import tn.esprit.Finance_It_Team_server.services.CalculServiceRemote;
import tn.esprit.Finance_It_Team_server.services.IRealData;
import tn.esprit.Finance_It_Team_server.services.VolatilityRemote;

public class CalculatePriceController implements Initializable {
	
	
	
	public void initialize(URL location, ResourceBundle resources) {

		
		ComboTypeOption.setItems(types);
		ComboModelOption.setItems(Models);

	}

	@FXML
	private DatePicker dateExpiry;
	@FXML
	private JFXButton btnHome;

	@FXML
	private JFXButton btCurrencyOption;

	@FXML
	private JFXButton btProfile;

	@FXML
	private JFXButton btforum;

	@FXML
	private JFXButton btnCalcul;

	@FXML
	private Label hello_text;

	@FXML
	private Button btnClose;

	@FXML
	private AnchorPane holderPane;

	@FXML
	private TextField riskTF;

	@FXML
	private TextField VolatiltyTF;

	@FXML
	private TextField CurrentPriceTF;

	@FXML
	private TextField StrikePriceTF;
	private ObservableList<TypeOption> typeOP = FXCollections.observableArrayList(TypeOption.call, TypeOption.put);

	@FXML
	private ComboBox<String> ComboTypeOption;

	@FXML
	private ComboBox<String> ComboModelOption;
	@FXML
	private CheckBox check_auto;
	 @FXML
	 private TextField DividendYieldTF;

	@FXML
	private Label price_label;
	@FXML
    private Button Greeks;

	ObservableList<RealData> data1 = FXCollections.observableArrayList();

	public static String typegraph;

	
	@FXML
	private Button Calculate_Price2;
	ObservableList<String> types = FXCollections.observableArrayList("call", "put");
	ObservableList<String> Models = FXCollections.observableArrayList("Black&Scholes", "Binomial", "Parity Put/Call");
	ObservableList<String> graphs = FXCollections.observableArrayList("long call", "short call", "long put",
			"short put");

	@FXML
	void CalculateOptionClicked(ActionEvent event) {

	}

	@FXML
	void CloseWindow(ActionEvent event) {
		Stage stage = (Stage) btnClose.getScene().getWindow();
		stage.close();

	}

	@FXML
	void CurrencyOptionClicked(ActionEvent event) throws IOException {
		try {

			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("../javafx/CurrencyOption.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

		} catch (IOException ex) {
			Logger.getLogger(Client_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@FXML
	void acceuil(ActionEvent event) {

	}

	@FXML
	void forum(ActionEvent event) {

	}

	public static double prix = 0;
	public static double a = 0;
	public static String g;
	public static Double c;
	public static double res;
	public static Double b;
	public static Double d;
	public static Double y;

	@FXML
	void price_click(ActionEvent event) throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/CalculService!tn.esprit.Finance_It_Team_server.services.CalculServiceRemote";

		Context context = new InitialContext();
		CalculServiceRemote proxy = (CalculServiceRemote) context.lookup(jndiName);

		boolean empty = (StrikePriceTF.getText().equals("") || CurrentPriceTF.getText().equals("")
				|| VolatiltyTF.getText().equals("") || riskTF.getText().equals(""));
		
		if (empty ) {
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Empty or Incorrect Field ");
			alert.setHeaderText("Complete All fields ");
			
			return;
		} 
		
		Calendar cal = Calendar.getInstance();
		Date today = cal.getTime();
		LocalDate n = dateExpiry.getValue();
		Date date = Date.from(n.atStartOfDay(ZoneId.systemDefault()).toInstant());
		long z = proxy.getDateDiff(today, date, TimeUnit.DAYS);
		
		res = (double) z / 365;
		

		a = Double.parseDouble(StrikePriceTF.getText());

		b = Double.parseDouble(CurrentPriceTF.getText());
		c = Double.parseDouble(VolatiltyTF.getText());
		
		d = Double.parseDouble(riskTF.getText());
		Double e = res;
		g = ComboTypeOption.getValue();
		y=Double.parseDouble(DividendYieldTF.getText());
		String h = ComboModelOption.getValue();

		String pri;
		
		if (e > 0) {

			if (g.equalsIgnoreCase("call") && h.equals("Black&Scholes")) {

				prix = proxy.callPrice(b, a, d, c, e);
				pri = Double.toString(prix);
				price_label.setText(pri);
			} else if (g.equalsIgnoreCase("put") && h.equals("Black&Scholes")) {
				prix = proxy.putPrice(b, a, d, c, e);
				pri = Double.toString(prix);
				price_label.setText(pri);

			} else if (g.equalsIgnoreCase("put") && h.equals("Binomial")) {
				prix = proxy.putOptionValue(b, a, e, c, e, y);
				pri = Double.toString(prix);
				price_label.setText(pri);
			} else if (g.equalsIgnoreCase("call") && h.equals("Binomial")) {
				prix = proxy.callOptionValue(b, a, e, c, e, y);
				pri = Double.toString(prix);
				price_label.setText(pri);
			} else if (g.equalsIgnoreCase("put") && h.equals("Parity Put/Call")) {
				prix = proxy.Parity(e, b, a, d, c, y, "put");
				pri = Double.toString(prix);
				price_label.setText(pri);
			} else if (g.equalsIgnoreCase("call") && h.equals("Parity Put/Call")) {
				prix = proxy.Parity(e, b, a, d, c, y, "call");
				pri = Double.toString(prix);
				price_label.setText(pri);
			}
		}

		else
			price_label.setText("Valeur Nulle");

	}

	@FXML
	void profile(ActionEvent event) throws IOException {
		try {
			// Button StockOption
			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("../javafx/ShowStockOption.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

		} catch (IOException ex) {
			Logger.getLogger(Client_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@FXML
	void graph_click(ActionEvent event) throws IOException, NamingException {

		calculate_volatility();
		Stage stage = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("../javafx/Chart.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}

	public double calculate_volatility() throws NamingException {
		String jndiName2 = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/RealDataRemote!tn.esprit.Finance_It_Team_server.services.IRealData";
		Context ctx2 = new InitialContext();
		IRealData proxy2 = (IRealData) ctx2.lookup(jndiName2);
		ArrayList<Float> allClose = new ArrayList<>();

		String jndiName3 = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/Volatility!tn.esprit.Finance_It_Team_server.services.VolatilityRemote";
		Context ctx3 = new InitialContext();
		VolatilityRemote proxy3 = (VolatilityRemote) ctx3.lookup(jndiName3);

		
		List<RealData> o = proxy2.getAllrealdata();
		for (RealData e : o) {
			allClose.add((float) e.getClose());
		}

		double d;
		d = proxy3.volatilty_value(allClose);
		return d;
	}

	@FXML
	void auto_click(MouseEvent event) throws NamingException {
		double o = calculate_volatility();
		String volnew = Double.toString(o);
		VolatiltyTF.setText(volnew);
		

	}
	@FXML
    void greeks_click(ActionEvent event) throws IOException,NamingException{
		Stage stage = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("../javafx/Greeks_Table.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

    }

	public double getopionvalue() {
		return prix;
	}

	public double getstrikeprice() {
		return a;
	}

	public String getoptiontype() {
		return g;
	}

	public String gettypegraph() {
		return typegraph;
	}
	public double getvolatility(){
		return c;
		
	}
	public double getcurrentprice(){
		return b;
	}
	public double gettime(){
		return res;
	}
	public double getrisk(){
		return d;
	}
	public double getdividend(){
		return y;
	}
}
