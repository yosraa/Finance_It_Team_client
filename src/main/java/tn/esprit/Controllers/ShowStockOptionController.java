/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.Controllers;

import com.jfoenix.controls.JFXButton;
import javafx.scene.control.Alert.AlertType;
import com.jfoenix.controls.JFXTextField;
import java.util.function.Predicate;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import notification.Notifications;
import notification.TrayNotification;
import java.awt.MenuItem;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.mail.MessagingException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;

import javafx.animation.Timeline;
import javafx.animation.KeyFrame;

//import org.controlsfx.control.Notifications;
import org.hibernate.mapping.Array;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.StateOptionStock;
import tn.esprit.Finance_It_Team_server.entities.Stock;
import tn.esprit.Finance_It_Team_server.entities.StockOption;
import tn.esprit.Finance_It_Team_server.entities.TypeOptionStock;
import tn.esprit.Finance_It_Team_server.services.IStockOptionService;
import tn.esprit.Finance_It_Team_server.services.StockOptionServiceRemote;

import tn.esprit.javafx.FXMLLoginController;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;

/**
 * FXML Controller class
 *
 * @author JEMMALI
 */
public class ShowStockOptionController implements Initializable {

	//private static final String Static = null;
	public static int idStockOption;

	private void TableFlush(TableView<StockOption> table) {
		for (int i = 0; i <= table.getItems().size(); i++) {
			table.getItems().clear();
		}
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {

		try {
			String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
			Context ctx = new InitialContext();
			IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);

			//List<StockOption> traders = proxy.getAllStockOption();
			List<StockOption> traders = proxy.findOptionsByTrader(FXMLLoginController.userId);
			StrikePriceC.setCellValueFactory(new PropertyValueFactory<>("strikePriceStock"));
			CurrentPriceC.setCellValueFactory(new PropertyValueFactory<>("currentPriceStock"));
			TypeOptionC.setCellValueFactory(new PropertyValueFactory<>("typeOptionStock"));
			RiskC.setCellValueFactory(new PropertyValueFactory<>("riskStock"));
			RateColumn.setCellValueFactory(new PropertyValueFactory<>("nbShares"));
			ObservableList<StockOption> items = FXCollections.observableArrayList(traders);
			TableStockOption.setItems(items);
			StockOption st = new StockOption();

			List<StockOption> o = proxy.findOptionsByTrader(FXMLLoginController.userId);
					//List<StockOption> o = proxy.getAllStockOption();
			for (StockOption e : o) {
				data1.add(e);
			}
			recherche.textProperty().addListener((observable, oldValue, newValue) -> {
				System.out.println("hit");
				Float v = new Float(newValue);
				data1.clear();
				data1.setAll(o.stream().filter(e -> e.getStrikePriceStock() >= v).collect(Collectors.toList()));
				TableStockOption.setItems(data1);
			});

		} catch (NamingException e) {

			e.printStackTrace();
		}
	}

	String idnew;
	@FXML
	private JFXButton btnHome;
	   @FXML
	    private Button logout;
	@FXML
	private JFXButton btCurrencyOption;
	@FXML
	private JFXButton btProfile;
	@FXML
	private JFXButton btforum;
	@FXML
	private JFXButton btnContacts;
	@FXML
	private Label hello_text;
	@FXML
	private Button btnClose;
	@FXML
	private AnchorPane holderPane;
	@FXML
	private TableView<StockOption> TableStockOption;
	@FXML
	private TableColumn<StockOption, ?> StrikePriceC;
	@FXML
	private TableColumn<StockOption, ?> CurrentPriceC;
	@FXML
	private TableColumn<StockOption, ?> TypeOptionC;
	@FXML
	private TableColumn<StockOption, ?> RiskC;
	@FXML
	private TableColumn<StockOption, ?> RateColumn;
	@FXML
	private Button btnRefresh;
	@FXML
	private Button btnUpdate;
	   @FXML
	    private TextField nbshares;

	    @FXML
	    private TextField nbtotstock;

	    @FXML
	    private TextField nbtotshares;

	    @FXML
	    private Button valeurStock;

	    @FXML
	    private Label valstock;

	@FXML
	private TextField recherche;
	@FXML
	private Button deleteOptionBtn;
	@FXML
	private TextField riskFT;
	@FXML
	private TextField CurrentP;
	@FXML
	private TextField StrikeP;
	@FXML
	private Button BtnBuyOption;
	  @FXML
	    private Button stockbtn;
	@FXML
	private Button BtnSellOption;

	@FXML
	private MenuItem call;

	@FXML
	private MenuItem put;
	@FXML
	private TextField SpotExchangeRate11;
	@FXML
	private Button addbutton;
	@FXML
	private TextField StateCurrencyCombo;
	ObservableList<StockOption> data = FXCollections.observableArrayList();

	@FXML
	private ComboBox<TypeOptionStock> ComboTypeOption;
	private ObservableList<TypeOptionStock> typeOP = FXCollections.observableArrayList(TypeOptionStock.call,
			TypeOptionStock.put);

	@FXML
	private TableView<StockOption> Tableview = new TableView<StockOption>();
	ObservableList<StockOption> data1 = FXCollections.observableArrayList();

	/**
	 * Initializes the controller class.
	 */

	@FXML
	private void acceuil(ActionEvent event) {
	}

	@FXML
	private void CurrencyOptionClicked(ActionEvent event) {
	}

	@FXML
	private void profile(ActionEvent event) {
	}

	@FXML
	private void forum(ActionEvent event) {
	}

	@FXML
	private void evenement(ActionEvent event) {
	}

	@FXML
	private void CloseWindow(ActionEvent event) {
	}

	@FXML
	private void btnRefresh(ActionEvent event) throws NamingException, IOException {
		/*
		 * TableStockOption.setItems(data1); RemplirTable();
		 * TableStockOption.refresh();
		 */

		try {

			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("../javafx/ShowStockOption.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.hide();
			stage.show();

		} catch (IOException ex) {
			Logger.getLogger(Client_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

  
	@FXML
	private void DeleteOption(ActionEvent event) throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);

		int idOption = TableStockOption.getSelectionModel().getSelectedItem().getId();
		proxy.deleteStockOption(idOption);
		List<StockOption> list = proxy.getAllStockOption();
		ObservableList<StockOption> items = FXCollections.observableArrayList(list);
		TableStockOption.setItems(items);

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Stock Option Removed");
		alert.setHeaderText("succesful");
		alert.showAndWait();
	}
    public static int userId;

	@FXML
	private void onclickAddSO(ActionEvent event) throws NamingException, SQLException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);

		StockOption st = new StockOption();
		st.setCurrentPriceStock(Float.parseFloat(CurrentP.getText()));
		st.setStrikePriceStock(Float.parseFloat(StrikeP.getText()));
		// st.setTypeOptionStock(ComboTypeOption.getValue());
		st.setRiskStock(Float.parseFloat(riskFT.getText()));
		st.setNbShares(Float.parseFloat(SpotExchangeRate11.getText()));

		//proxy.addStockOption(st);

		proxy.addStockOption(st,FXMLLoginController.userId);
		System.out.println(FXMLLoginController.userId);

		
		TableStockOption.setItems(data1);

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Stock Option Adding");
		alert.setHeaderText("Stock Option Added");
		alert.showAndWait();
		RemplirTable();

	}

	public void RemplirTable() throws SQLException, NamingException {

		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);

		StrikePriceC.setCellValueFactory(new PropertyValueFactory<>("strikePriceStock"));
		CurrentPriceC.setCellValueFactory(new PropertyValueFactory<>("currentPriceStock"));
		TypeOptionC.setCellValueFactory(new PropertyValueFactory<>("typeOptionStock"));
		RiskC.setCellValueFactory(new PropertyValueFactory<>("riskStock"));
		RateColumn.setCellValueFactory(new PropertyValueFactory<>("nbShares"));
		StockOption c = new StockOption();
	//	List<StockOption> o = proxy.getAllStockOption();
	//	List<StockOption> o = proxy.getAllStockOptionNotATrader(FXMLLoginController.userId);
		List<StockOption> o = proxy.getIdeesByUserId(FXMLLoginController.userId);

		for (StockOption e : o) {
			data1.add(e);
		}
		TableStockOption.setItems(data1);
	}

	void showPubDetails(StockOption e) throws ParseException {

		idnew = Integer.toString(e.getId());

		StrikeP.setText(StrikeP.getText().toString());
		CurrentP.setText(CurrentPriceC.getText().toString());
		riskFT.setText(RiskC.getText().toString());
		SpotExchangeRate11.setText(RateColumn.getText().toString());
	}
	@FXML
    void OnClickValeurStock(ActionEvent event) throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);
		
		boolean empty =( nbshares.getText().equals("")&& nbtotshares.getText().equals("") && nbtotstock.getText().equals(""));
		if (empty == true) {
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Empty or Incorrect Field ");
			alert.setHeaderText("Complete All fields ");
			Optional<ButtonType> result = alert.showAndWait();
			return;
		}

	
		Float a = Float.parseFloat(nbshares.getText());
		Float b = Float.parseFloat(nbtotshares.getText());
		Float c = Float.parseFloat(nbtotstock.getText());

		Float prix = proxy.calculValueShares(a, b, c);
		
		String title = "Value of your Share";
		//String message = "You've successfully received an EMAIL that you BOAT an option, it's now on hold of confirmation.";
		Notifications notification = Notifications.SUCCESS;
		TrayNotification tray = new TrayNotification();
		tray.setTitle(title);
		//tray.setMessage(message);
		tray.setNotification(notification);
		tray.showAndWait();
		
		String pri = Float.toString(prix);
		valstock.setText(pri);
    }
	@FXML
	void btnUpdate(ActionEvent event) throws NamingException, SQLException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);

		if (TableStockOption.getSelectionModel().getSelectedItem() != null) {
			StockOption r = TableStockOption.getSelectionModel().getSelectedItem();
			r.setStrikePriceStock(Float.parseFloat(StrikeP.getText()));
			r.setCurrentPriceStock(Float.parseFloat(CurrentP.getText()));
			r.setRiskStock(Float.parseFloat(riskFT.getText()));
			r.setNbShares(Float.parseFloat(SpotExchangeRate11.getText()));
			proxy.updateStockOption(r);
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Stock Option updated");
			alert.setHeaderText("succesful");
			alert.showAndWait();

			// TableFlush(TableStockOption);

			TableStockOption.getItems().clear();
			TableStockOption.refresh();
			TableStockOption.setItems(data1);

			RemplirTable();
		}

	}
	EntityManager em;
	@FXML
	void mouse() {
		if (TableStockOption.getSelectionModel().getSelectedItem() != null) {
			StockOption r = TableStockOption.getSelectionModel().getSelectedItem();
		 
			StrikeP.setText(r.getStrikePriceStock() + "");
			CurrentP.setText(r.getCurrentPriceStock() + "");
			riskFT.setText(r.getRiskStock() + "");
			SpotExchangeRate11.setText(r.getNbShares() + "");
		
			nbshares.setText(r.getNbShares() + "");
			//nbshares.setText(r.getId() + "");

		}

	}

	@FXML
	void onclickSell(ActionEvent event) throws MessagingException {
		try {

			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("../javafx/EquityTradeOptionSHOW.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

		} catch (IOException ex) {
			Logger.getLogger(ShowStockOptionController.class.getName()).log(Level.SEVERE, null, ex);
		}
		SendMail s = new SendMail("[PIDEV][API][MAIL]","myriammalek.jemmali@esprit.tn","For your privacy Sir/Madame we informe you that you have sold an option");
		
	}

    @FXML
    void onclickStock(ActionEvent event) {
    	try {

			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/YahooFinanceHistoricalDataMalek/HomeManagement.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

		} catch (IOException ex) {
			Logger.getLogger(ShowStockOptionController.class.getName()).log(Level.SEVERE, null, ex);
		}
    }
	@FXML
	void OnClickBuy(ActionEvent event) throws NamingException, MessagingException {

		try {

			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("../javafx/EquityTradeOptionSHOW.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		
		} catch (IOException ex) {
			Logger.getLogger(ShowStockOptionController.class.getName()).log(Level.SEVERE, null, ex);
		}
		SendMail s = new SendMail("[PIDEV][API][MAIL]","myriammalek.jemmali@esprit.tn","For your privacy Sir/Madame we informe you that you have boat an option");
		String title = "Check Your Email";
		String message = "Congratulation you bought an OPTION";
		Notifications notification = Notifications.SUCCESS;
		TrayNotification tray = new TrayNotification();
		tray.setTitle(title);
		tray.setMessage(message);
		tray.setNotification(notification);
		tray.showAndWait();
	}


	@FXML
	void SetToCall(ActionEvent event) throws NamingException, SQLException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);
		if (TableStockOption.getSelectionModel().getSelectedItem() != null) {
			StockOption r = TableStockOption.getSelectionModel().getSelectedItem();
			r.setTypeOptionStock(TypeOptionStock.call);
			proxy.updateStockOption(r);
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Type Option updated to CALL ");
			alert.setHeaderText("succesful");
			alert.showAndWait();
			 TableFlush(TableStockOption);

			/*TableStockOption.getItems().clear();
			TableStockOption.refresh();
			TableStockOption.setItems(data1);*/
			RemplirTable();
		}
	}

	@FXML
	void SetToPut(ActionEvent event) throws NamingException, SQLException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);
		if (TableStockOption.getSelectionModel().getSelectedItem() != null) {
			StockOption r = TableStockOption.getSelectionModel().getSelectedItem();
			r.setTypeOptionStock(TypeOptionStock.put);
			proxy.updateStockOption(r);
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Type Option updated to PUT ");
			alert.setHeaderText("succesful");
			alert.showAndWait();

			 TableFlush(TableStockOption);

			/*TableStockOption.getItems().clear();
			TableStockOption.refresh();
			TableStockOption.setItems(data1);*/
			RemplirTable();
		}

	}
	@FXML
    void logout(ActionEvent event)throws NamingException, IOException {
    	Stage stage = (Stage) logout.getScene().getWindow();
		stage.close();
    }
}
