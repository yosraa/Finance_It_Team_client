package tn.esprit.Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.services.IOptionProduct;
import tn.esprit.javafx.FXMLLoginController;

public class CurrencyOptionOfTraderConnectedController implements Initializable {
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			RemplirTable();
		} catch (SQLException e) {
			Logger.getLogger(CurrencyOptionController.class.getName()).log(Level.SEVERE, null, e);
			e.printStackTrace();
		} catch (NamingException e) {

			e.printStackTrace();
		}

	}

	@FXML
	private JFXButton btnHome;

	@FXML
	private JFXButton btCurrencyOption;

	@FXML
	private JFXButton btProfile;

	@FXML
	private JFXButton btforum;

	@FXML
	private JFXButton btnContacts;

	@FXML
	private Label hello_text;

	@FXML
	private Button btnClose;

	@FXML
	private AnchorPane holderPane;

	@FXML
	private Button searchBtn;
    @FXML
    private Button goBack;

	@FXML
	private TableView<CurrencyOption> TableCurrencyOption;

	@FXML
	private TableColumn<CurrencyOption, ?> StrikePriceColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> CurrentPriceColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> TypeOptionColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> volatilityColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> RiskColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> TimeColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> UnitExchangeColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> RateColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> StateCurrencyColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> ExpirationDateColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> PremiumColumn;


	@FXML
	private ComboBox<?> comboSearch;

	
	@FXML
	private VBox RectangleAffiche;

	@FXML
	private Label gainOrLoss;

	@FXML
	private Label labelGain;

	@FXML
	private Label labelLoss;

	@FXML
	private ImageView gain;

	@FXML
	private ImageView loss;

	ObservableList<CurrencyOption> data2 = FXCollections.observableArrayList();

	@FXML
	void CloseWindow(ActionEvent event) {

	}


	@FXML
	void acceuil(ActionEvent event) {

	}

	@FXML
	void evenement(ActionEvent event) {

	}

	@FXML
	void forum(ActionEvent event) {

	}

	
	 @FXML
	    void back(ActionEvent event) {
		  Stage stage = (Stage) goBack.getScene().getWindow();
			stage.close();
	    }

	@FXML
	void profile(ActionEvent event) {

	}

	@FXML
	void searchClick(ActionEvent event) {

	}

	@FXML
	void selectItem(MouseEvent event) {

	}

	public void RemplirTable() throws SQLException, NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";
		Context ctx = new InitialContext();
		IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);
		CurrentPriceColumn.setCellValueFactory(new PropertyValueFactory<>("currentPrice"));
		TypeOptionColumn.setCellValueFactory(new PropertyValueFactory<>("Typeoption"));
		ExpirationDateColumn.setCellValueFactory(new PropertyValueFactory<>("expirationDate"));
		RiskColumn.setCellValueFactory(new PropertyValueFactory<>("risk"));
		RateColumn.setCellValueFactory(new PropertyValueFactory<>("spotExchangeRate"));
		StateCurrencyColumn.setCellValueFactory(new PropertyValueFactory<>("StateOption"));
		StrikePriceColumn.setCellValueFactory(new PropertyValueFactory<>("strikePrice"));
		TimeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));
		UnitExchangeColumn.setCellValueFactory(new PropertyValueFactory<>("UnitEchange"));
		volatilityColumn.setCellValueFactory(new PropertyValueFactory<>("volatility"));
		PremiumColumn.setCellValueFactory(new PropertyValueFactory<>("premium"));
		System.out.println(FXMLLoginController.userId);
		List<CurrencyOption> o = proxy.getAllCurrencyOptionIdTrader(FXMLLoginController.userId);
		for (CurrencyOption e : o) {
			data2.add(e);
		}
		TableCurrencyOption.setItems(data2);
	}

}
