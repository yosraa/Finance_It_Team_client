package tn.esprit.Controllers;

import javafx.scene.control.TextArea;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import tn.esprit.Finance_It_Team_server.entities.AchatCurrencyOption;
import tn.esprit.Finance_It_Team_server.services.AchatCurrencyOptionRemote;
import tn.esprit.Finance_It_Team_server.services.SendMail;

public class AdminTradingForexOptionController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		try {
			RemplirTable3();
		} catch (SQLException e) {

			e.printStackTrace();
		} catch (NamingException e) {

			e.printStackTrace();
		}
	}

	@FXML
	private JFXButton btnHome;

	@FXML
	private JFXButton btCurrencyOption;

	@FXML
	private JFXButton btProfile;

	@FXML
	private JFXButton btforum;

	@FXML
	private JFXButton btnContacts;

	@FXML
	private Label hello_text;

	@FXML
	private Button btnClose;

	@FXML
	private AnchorPane holderPane;

	@FXML
	private Button Traiter;

	@FXML
	private JFXTextField searchFT;

	@FXML
	private Button searchBtn;

	@FXML
	private TextField TFusername;

	@FXML
	private TextField TFemail;

	@FXML
	private Button sendmail;
	@FXML
	private TextArea message;

	@FXML
	private TableView<AchatCurrencyOption> TableAdminTrading;
	ObservableList<AchatCurrencyOption> data = FXCollections.observableArrayList();

	@FXML
	private TableColumn<AchatCurrencyOption, ?> SettelementDateColumn;

	@FXML
	private TableColumn<AchatCurrencyOption, ?> stateColumnTrade;

	@FXML
	void CloseWindow(ActionEvent event) {

	}

	@FXML
	void CurrencyOptionClicked(ActionEvent event) {

	}

	@FXML
	void acceuil(ActionEvent event) {

	}

	@FXML
	void evenement(ActionEvent event) {

	}

	@FXML
	void forum(ActionEvent event) {

	}

	@FXML
	void onclickTraiter(ActionEvent event) {
		  Stage stage = (Stage) Traiter.getScene().getWindow();
				stage.close();
	}

	@FXML
	void profile(ActionEvent event) {

	}

	@FXML
	void searchClick(ActionEvent event) {

	}

	public void RemplirTable3() throws SQLException, NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/AchatCurrencyOptionService!tn.esprit.Finance_It_Team_server.services.AchatCurrencyOptionRemote";
		Context ctx1 = new InitialContext();
		AchatCurrencyOptionRemote proxy = (AchatCurrencyOptionRemote) ctx1.lookup(jndiName);
		SettelementDateColumn.setCellValueFactory(new PropertyValueFactory<>("settelmentDate"));
		stateColumnTrade.setCellValueFactory(new PropertyValueFactory<>("stateTrade"));
		List<AchatCurrencyOption> achatCurrencyOption = proxy.getAllTradePending();
		System.out.println(achatCurrencyOption);
		for (AchatCurrencyOption a : achatCurrencyOption) {

			data.add(a);
		}
		TableAdminTrading.setItems(data);
	}

	@FXML
	void replay(ActionEvent event) {
		AchatCurrencyOption achat = TableAdminTrading.getSelectionModel().getSelectedItem();

		SendMail.send(achat.getTrader().getEmail(), "Trade request treatment in process", message.getText(),
				"tarhouniyosra11@gmail.com", "yesmine95");
		System.out.println("aaaaaa");

	}

	@FXML
	void selected(MouseEvent event) throws NamingException {
		if (TableAdminTrading.getSelectionModel().getSelectedItem() != null) {
			AchatCurrencyOption achat = TableAdminTrading.getSelectionModel().getSelectedItem();
			TFusername.setText(achat.getTrader().getFirstName());
			TFemail.setText(achat.getTrader().getEmail());

		}

	}
}
