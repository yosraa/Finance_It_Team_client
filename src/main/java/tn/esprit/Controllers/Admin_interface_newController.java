package tn.esprit.Controllers;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;



public class Admin_interface_newController implements Initializable {

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	
	  @FXML
	    private JFXButton ValidateTrader;

	    @FXML
	    private JFXButton ValidateForexOption;

	    @FXML
	    private JFXButton TreatForexTrade;

	    @FXML
	    private JFXButton stockOption;

	    @FXML
	    private JFXButton ClaimsArea;

	    @FXML
	    private Label hello_text;

	    @FXML
	    private Button btnClose;

	    @FXML
	    private AnchorPane holderPane;

	    @FXML
	    void ClaimsAreaClicked(ActionEvent event) throws IOException {
	    	Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("../javafx/FXMLReclamationAdmin.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

	    }

	    @FXML
	    void CloseWindow(ActionEvent event) {

	    }

	    @FXML
	    void TreatForexTradeClicked(ActionEvent event) {
	    	try {

				Stage stage = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("../javafx/AdminTrading1.fxml"));
				Scene scene = new Scene(root);
				stage.setScene(scene);
				stage.show();

			} catch (IOException ex) {
				Logger.getLogger(Admin_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
			}
	    }

	    @FXML
	    void ValidateForexOptionClicked(ActionEvent event) {
	    	try {

				Stage stage = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("../javafx/AdminCurrencyOption.fxml"));
				Scene scene = new Scene(root);
				stage.setScene(scene);
				stage.show();

			} catch (IOException ex) {
				Logger.getLogger(Admin_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
			}
	    }

	    @FXML
	    void ValidateTraderclick(ActionEvent event) {
	    	try {

				Stage stage = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("../javafx/FXMLAdmin.fxml"));
				Scene scene = new Scene(root);
				stage.setScene(scene);
				stage.show();

			} catch (IOException ex) {
				Logger.getLogger(Admin_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
			}
	    }

	    @FXML
	    void stockOptionClicked(ActionEvent event) {
	    	try {

				Stage stage = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("../javafx/AdminOption.fxml"));
				Scene scene = new Scene(root);
				stage.setScene(scene);
				stage.show();

			} catch (IOException ex) {
				Logger.getLogger(Admin_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
			}
	    }
}   
	 












