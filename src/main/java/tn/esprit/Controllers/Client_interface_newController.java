package tn.esprit.Controllers;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import tn.esprit.Finance_It_Team_server.entities.RealData;
import tn.esprit.Finance_It_Team_server.services.IRealData;



public class Client_interface_newController implements Initializable {

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	 @FXML
	    private JFXButton btnreal;
	 
	@FXML
	private JFXButton btnHome;

	@FXML
	private JFXButton btCurrencyOption;

	@FXML
	private JFXButton btProfile;
	@FXML
	private JFXButton btProfile1;
	@FXML
	private JFXButton Profile1;
	@FXML
	private JFXButton btforum;

	@FXML
	private JFXButton btnContacts;

	@FXML
	private Label hello_text;

	@FXML
	private Button btnClose;

	@FXML
	private AnchorPane holderPane;
	@FXML
	private Button archievebtn;
	@FXML
	private Button statistic;

	@FXML
	private Button traderArea;
    @FXML
    private Button btnClaimsTrader;

	@FXML
	void CurrencyOptionClicked(ActionEvent event) throws IOException {
		try {

			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("../javafx/CurrencyOption.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

		} catch (IOException ex) {
			Logger.getLogger(Client_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@FXML
	void acceuil(ActionEvent event) {

	}

	@FXML
	void evenement(ActionEvent event) {

	}
	   @FXML
	    void ClaimsTraderClicked(ActionEvent event) {
		   try {

				Stage stage = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("../javafx/FXMLReclamationTrader.fxml"));
				Scene scene = new Scene(root);
				stage.setScene(scene);
				stage.show();

			} catch (IOException ex) {
				Logger.getLogger(Client_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
			}
	    }
	@FXML
	void profile(ActionEvent event) throws IOException {
		try {

			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("../javafx/ShowStockOption.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

		} catch (IOException ex) {
			Logger.getLogger(Client_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	@FXML
	void profile1(ActionEvent event) throws IOException {
		try {

			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/YahooFinanceHistoricalDataMalek/HomeManagement.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

		} catch (IOException ex) {
			Logger.getLogger(Client_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	@FXML
	void archieve(ActionEvent event) throws IOException {
		try {

			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("../javafx/ArchiveCurrencyOption2.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

		} catch (IOException ex) {
			Logger.getLogger(Client_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	@FXML
	void statisticCliked(ActionEvent event) {
		try {

			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("../javafx/StatisticCurrencyOption.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

		} catch (IOException ex) {
			Logger.getLogger(Client_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	@FXML
	void CalculateOptionClicked(ActionEvent event) throws IOException {
		try {

			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("../javafx/Option_Price.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
			Stage stage2 = (Stage) btnClose.getScene().getWindow();
			stage2.close();

		} catch (IOException ex) {
			Logger.getLogger(Client_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	@FXML
	void forum(ActionEvent event) throws IOException {

		Stage stage = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("../javafx/CurrencyAccount.fxml"));
		Scene scene = new Scene(root);

		stage.setScene(scene);
		stage.show();

	}

	@FXML
	void CloseWindow(ActionEvent event) {

		Stage stage = (Stage) btnClose.getScene().getWindow();
		stage.close();

	}

	@FXML
	void afficherOptionTraderAdd(ActionEvent event) {
		try {

			Stage stage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("../javafx/CurrencyOptionOfTraderConnected.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

		} catch (IOException ex) {
			Logger.getLogger(Client_interface_newController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	@FXML
    void real(ActionEvent event) throws IOException, NamingException {
    	
    	Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../javafx/RealTimeData.fxml"));
        Scene scene = new Scene(root);
        //buildStocks();
        
        stage.setScene(scene);
        stage.show();
            Stage stage2 = (Stage) btnClose.getScene().getWindow();
            stage2.close();
    	
    	
    	/*
  		URL url= new URL("https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=MSFT&interval=1min&apikey=demo");
  		
  		URLConnection urlconn=url.openConnection();
  		InputStreamReader inStream=new InputStreamReader(urlconn.getInputStream());
  		BufferedReader bff=new BufferedReader(inStream);
  		String line=bff.readLine();
  		while(line!=null){
  			
  			System.out.println(line);
  			line=bff.readLine();*/

    
    }


		


}  

	 












