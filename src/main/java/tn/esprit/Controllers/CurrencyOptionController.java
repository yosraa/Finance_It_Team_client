package tn.esprit.Controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.StateOption;
import tn.esprit.Finance_It_Team_server.entities.TypeOption;
import tn.esprit.Finance_It_Team_server.entities.UnitExchange;
import tn.esprit.Finance_It_Team_server.services.CalculServiceRemote;
import tn.esprit.Finance_It_Team_server.services.IOptionProduct;
import tn.esprit.javafx.FXMLLoginController;

public class CurrencyOptionController implements Initializable {

	public static int idCurrencyOption;
	public static int idTraderSell;
	public static float strikePrice;
	public static Date dateExpiration;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		unitExchangecombo.setItems(unitex);
		ComboTypeOption.setItems(typeOP);
		ComboStateCurrency.setItems(StateOP);
		comboSearch.setItems(typeSE);
		try {
			RemplirTable();
		} catch (SQLException e) {
			Logger.getLogger(CurrencyOptionController.class.getName()).log(Level.SEVERE, null, e);
			e.printStackTrace();
		} catch (NamingException e) {

			e.printStackTrace();
		}

	}

	@FXML
	private JFXButton btnHome;

	@FXML
	private JFXButton btCurrencyOption;

	@FXML
	private JFXButton btProfile;

	@FXML
	private JFXButton btforum;

	@FXML
	private JFXButton btnContacts;

	@FXML
	private Label hello_text;

	@FXML
	private Button btnClose;

	@FXML
	private AnchorPane holderPane;

	@FXML
	private TextField StateCurrencyCombo;

	@FXML
	private TextField SpotExchangeRate;

	@FXML
	private TextField timeTF;

	@FXML
	private TextField riskFT;

	@FXML
	private TextField VolatiltyTF;

	@FXML
	private TextField CurrentP;
	@FXML
	private Button archivage;

	@FXML
	private TextField StrikeP;

	@FXML
	private VBox RectangleAffiche;
    @FXML
    private Button goBack;

	private ObservableList<UnitExchange> unitex = FXCollections.observableArrayList(UnitExchange.EUR_TND,
			UnitExchange.JPY_EUR, UnitExchange.USD_EUR, UnitExchange.CAD_EUR, UnitExchange.USD_TDN);
	@FXML
	private ComboBox<UnitExchange> unitExchangecombo;

	@FXML
	private DatePicker dateExpiry;
	private ObservableList<TypeOption> typeOP = FXCollections.observableArrayList(TypeOption.call, TypeOption.put);

	@FXML
	private ComboBox<TypeOption> ComboTypeOption;

	private ObservableList<StateOption> StateOP = FXCollections.observableArrayList(StateOption.available,
			StateOption.unavailable);
	@FXML
	private ComboBox<StateOption> ComboStateCurrency;

	@FXML
	private Button addbutton;

	@FXML
	private Button reset;

	private ObservableList<UnitExchange> typeSE = FXCollections.observableArrayList(UnitExchange.EUR_TND,
			UnitExchange.JPY_EUR, UnitExchange.USD_EUR, UnitExchange.CAD_EUR, UnitExchange.USD_TDN);

	@FXML
	private ComboBox<UnitExchange> comboSearch;
	ObservableList<CurrencyOption> data2 = FXCollections.observableArrayList();
	ObservableList<CurrencyOption> data = FXCollections.observableArrayList();

	@FXML
	private TableView<CurrencyOption> TableCurrencyOption;

	@FXML
	private TableColumn<CurrencyOption, ?> StrikePriceColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> CurrentPriceColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> TypeOptionColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> volatilityColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> RiskColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> TimeColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> UnitExchangeColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> RateColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> StateCurrencyColumn;

	@FXML
	private TableColumn<CurrencyOption, ?> ExpirationDateColumn;
	@FXML
	private TableColumn<CurrencyOption, ?> PremiumColumn;

	@FXML
	private Button searchBtn;

	@FXML
	private Label gainOrLoss;

	@FXML
	private ImageView gain;

	@FXML
	private ImageView loss;

	@FXML
	private Label labelGain;

	@FXML
	private Label labelLoss;

	@FXML
	private Button btnBuy;

	
	@FXML
	private Label anteriorDate;

	@FXML
	void CloseWindow(ActionEvent event) {

	}

	@FXML
	void CurrencyOptionClicked(ActionEvent event) {

	}

	@FXML
	void acceuil(ActionEvent event) {

	}

	@FXML
	void evenement(ActionEvent event) {

	}
	  @FXML
	    void back(ActionEvent event) {
		  Stage stage = (Stage) goBack.getScene().getWindow();
			stage.close();
	    }
	@FXML
	void forum(ActionEvent event) {

	}

	@FXML
	private Button GainOrLossBtn;

	@FXML
	void reset(ActionEvent event) {
		StrikeP.clear();
		ComboStateCurrency.setValue(null);
		CurrentP.clear();
		riskFT.clear();
		timeTF.clear();
		SpotExchangeRate.clear();
		dateExpiry.setValue(null);
		VolatiltyTF.clear();
		unitExchangecombo.setValue(null);
		ComboTypeOption.setValue(null);
	}

	@FXML
	void profile(ActionEvent event) {

	}

	@FXML
	void GainOrLossClicked(ActionEvent event) throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";
		Context ctx = new InitialContext();
		IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);

		float a = proxy.gainOrLossOfTraderCallAndPut(TableCurrencyOption.getSelectionModel().getSelectedItem().getId());
		RectangleAffiche.setVisible(true);
		if (a >= 0) {
			gain.setVisible(true);
			loss.setVisible(false);

			gainOrLoss.setText(String.valueOf(a) + "  you should exercice this contract");
		}
		if (a < 0) {
			gain.setVisible(false);
			loss.setVisible(true);

			gainOrLoss.setText(String.valueOf(a) + "  you shoudn't exercice this contract");
		}

	}

	@FXML
	void OnClickBuy(ActionEvent event) throws NamingException {

		if (TableCurrencyOption.getSelectionModel().getSelectedItem() != null) {
			idCurrencyOption = TableCurrencyOption.getSelectionModel().getSelectedItem().getId();
			idTraderSell=TableCurrencyOption.getSelectionModel().getSelectedItem().getTrader().getId();
			System.out.println(idTraderSell);
			strikePrice = TableCurrencyOption.getSelectionModel().getSelectedItem().getStrikePrice();
			dateExpiration = TableCurrencyOption.getSelectionModel().getSelectedItem().getExpirationDate();
			System.out.println(strikePrice);
			System.out.println(dateExpiration);
		
			Calendar calendar = new GregorianCalendar();
			int year = calendar.get(Calendar.YEAR);
		
			try {

				Stage stage = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("../javafx/ForexTradingShow.fxml"));
				Scene scene = new Scene(root);
				stage.setScene(scene);
				
				stage.show();
				 

			} catch (IOException ex) {
				Logger.getLogger(CurrencyOptionController.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

	}



	@FXML
	void onclickArchivage(ActionEvent event) throws SQLException, NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";
		Context ctx = new InitialContext();
		IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);
		CurrencyOption c = TableCurrencyOption.getSelectionModel().getSelectedItem();
		if (TableCurrencyOption.getSelectionModel().getSelectedItem().getArchive() == 0) {

			c.setArchive(1);
			proxy.updateOption(c);
			List<CurrencyOption> o = proxy.getCurrencyOptionById();
			for (CurrencyOption e : o) {
				data.add(e);
			}

			TableCurrencyOption.getItems().clear();
			RemplirTable();
		}

	}

	@FXML
	void onclicksubmit(ActionEvent event) throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";
		Context ctx = new InitialContext();
		IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);

		/******************* Calcul ********************/
		String jndiName2 = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/CalculService!tn.esprit.Finance_It_Team_server.services.CalculServiceRemote";

		Context context2 = new InitialContext();
		CalculServiceRemote proxy2 = (CalculServiceRemote) context2.lookup(jndiName2);
		/*****************************************/

		CurrencyOption optionp = new CurrencyOption();
		Double current, strike, time, risk, volatility;
		TypeOption type;
		current = Double.parseDouble(CurrentP.getText());
		strike = Double.parseDouble(StrikeP.getText());
		time = Double.parseDouble(timeTF.getText());
		risk = Double.parseDouble(riskFT.getText());
		volatility = Double.parseDouble(VolatiltyTF.getText());
		type = ComboTypeOption.getValue();

		optionp.setCurrentPrice(Float.parseFloat(CurrentP.getText()));
		optionp.setStrikePrice(Float.parseFloat(StrikeP.getText()));
		optionp.setTypeoption(ComboTypeOption.getValue());
		optionp.setUnitEchange(unitExchangecombo.getValue());
		optionp.setTime(Float.parseFloat(timeTF.getText()));
		optionp.setRisk(Float.parseFloat(riskFT.getText()));
		optionp.setVolatility(Float.parseFloat(VolatiltyTF.getText()));
		optionp.setSpotExchangeRate(Float.parseFloat(SpotExchangeRate.getText()));
		optionp.getStateOption();
		ComboStateCurrency.setValue(StateOption.unavailable);
		optionp.setStateOption(ComboStateCurrency.getValue());
		optionp.setArchive(0);

		Double price;

		if (type == TypeOption.call) {
			price = proxy2.callPrice(current, strike, risk, volatility, time);

		} else {
			price = proxy2.putPrice(current, strike, risk, volatility, time);

		}

		optionp.setPremium(price);
		LocalDate a = dateExpiry.getValue();
		java.sql.Date d = java.sql.Date.valueOf(a);

		java.util.Date da = new java.util.Date();

		if (d.after(da)) {
			optionp.setExpirationDate(d);
		} else {
			System.out.println("error date");
			// anteriorDate.setVisible(true);
		}

		if (!(timeTF.getText().equals("")) && !(ComboTypeOption.getValue().equals("")) && !(riskFT.getText().equals(""))
				&& !(SpotExchangeRate.getText().equals("")) && !(ComboStateCurrency.getValue().equals(""))
				&& !(StrikeP.getText().equals("")) && !(VolatiltyTF.getText().equals(""))
				&& !(unitExchangecombo.getValue().equals("")) && !(CurrentP.getText().equals(""))
				&& !(dateExpiry.getValue().equals("")) && (d.after(da))) {
			proxy.addOptionProduct(optionp, FXMLLoginController.userId);
			System.out.println(FXMLLoginController.userId);
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Successfully added ");
			alert.setHeaderText("Adding a succesful currency option");
			Optional<ButtonType> result = alert.showAndWait();
			TableCurrencyOption.getItems().clear();
			try {
				RemplirTable();
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

		else {
			anteriorDate.setVisible(true);
			Alert alertE = new Alert(Alert.AlertType.INFORMATION);
			alertE.setTitle("Addition Error ");
			alertE.setHeaderText("No Addition of Currency Option :error Date");
			Optional<ButtonType> result = alertE.showAndWait();
		}

	}

	@FXML
	void onclick(ActionEvent event) {

	}

	public void RemplirTable() throws SQLException, NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";
		Context ctx = new InitialContext();
		IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);
		CurrentPriceColumn.setCellValueFactory(new PropertyValueFactory<>("currentPrice"));
		TypeOptionColumn.setCellValueFactory(new PropertyValueFactory<>("Typeoption"));
		ExpirationDateColumn.setCellValueFactory(new PropertyValueFactory<>("expirationDate"));
		RiskColumn.setCellValueFactory(new PropertyValueFactory<>("risk"));
		RateColumn.setCellValueFactory(new PropertyValueFactory<>("spotExchangeRate"));
		StateCurrencyColumn.setCellValueFactory(new PropertyValueFactory<>("StateOption"));
		StrikePriceColumn.setCellValueFactory(new PropertyValueFactory<>("strikePrice"));
		TimeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));
		UnitExchangeColumn.setCellValueFactory(new PropertyValueFactory<>("UnitEchange"));
		volatilityColumn.setCellValueFactory(new PropertyValueFactory<>("volatility"));
		PremiumColumn.setCellValueFactory(new PropertyValueFactory<>("premium"));

		System.out.println(FXMLLoginController.userId);
		List<CurrencyOption> o = proxy.getAllCurrencyOptionNotIdTrader(FXMLLoginController.userId);

		for (CurrencyOption e : o) {
			data.add(e);
		}
		TableCurrencyOption.setItems(data);

	}

	@FXML
	void searchClick(ActionEvent event) throws NamingException, SQLException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";
		Context ctx = new InitialContext();
		IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);
		CurrentPriceColumn.setCellValueFactory(new PropertyValueFactory<>("currentPrice"));
		TypeOptionColumn.setCellValueFactory(new PropertyValueFactory<>("Typeoption"));
		ExpirationDateColumn.setCellValueFactory(new PropertyValueFactory<>("expirationDate"));
		RiskColumn.setCellValueFactory(new PropertyValueFactory<>("risk"));
		RateColumn.setCellValueFactory(new PropertyValueFactory<>("spotExchangeRate"));
		StateCurrencyColumn.setCellValueFactory(new PropertyValueFactory<>("StateOption"));
		StrikePriceColumn.setCellValueFactory(new PropertyValueFactory<>("strikePrice"));
		TimeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));
		UnitExchangeColumn.setCellValueFactory(new PropertyValueFactory<>("UnitEchange"));
		volatilityColumn.setCellValueFactory(new PropertyValueFactory<>("volatility"));
		List<CurrencyOption> listo = proxy.findOptionByUnitExchange(comboSearch.getValue());

		for (CurrencyOption e : listo) {

			data2.add(e);
			System.out.println(e);
		}

		TableCurrencyOption.setItems(data2);

	}

	@FXML
	void selectItem(MouseEvent event) throws ParseException {

		if (TableCurrencyOption.getSelectionModel().getSelectedItem() != null) {
			CurrencyOption r = TableCurrencyOption.getSelectionModel().getSelectedItem();

			ComboTypeOption.setValue(r.getTypeoption());
			ComboStateCurrency.setValue(r.getStateOption());
			unitExchangecombo.setValue(r.getUnitEchange());
			timeTF.setText(r.getTime() + "");
			StrikeP.setText(r.getStrikePrice() + "");
			CurrentP.setText(r.getCurrentPrice() + "");
			SpotExchangeRate.setText(r.getSpotExchangeRate() + "");
			// r.setExpirationDate(java.sql.Date.valueOf(dateExpiry.setValue()));
			riskFT.setText(r.getRisk() + "");
			VolatiltyTF.setText(r.getVolatility() + "");

		}

	}
}
