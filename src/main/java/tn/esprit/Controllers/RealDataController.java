/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.Controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import tn.esprit.Finance_It_Team_server.entities.CurrencyAccount;
import tn.esprit.Finance_It_Team_server.entities.RealData;
import tn.esprit.Finance_It_Team_server.services.ICurrencyAccount;
import tn.esprit.Finance_It_Team_server.services.IOptionProduct;
import tn.esprit.Finance_It_Team_server.services.IRealData;

/**
 * FXML Controller class
 *
 * @author Amine
 */
public class RealDataController implements Initializable {

    @FXML
    private Label hello_text;
    @FXML
    private Button btnClose;
    @FXML
    private AnchorPane holderPane;
    @FXML
    private Button Foreignbutton;
    @FXML
    private Button currencybutton11;
    @FXML
    private Button stockbutton1;
    @FXML
    private TableView<RealData> Tableview1;
    @FXML
    private TableColumn<?, ?> namecol;
    @FXML
    private TableColumn<?, ?> opencol;
    @FXML
    private TableColumn<?, ?> highcol;
    @FXML
    private TableColumn<?, ?> lowcol;
    @FXML
    private TableColumn<?, ?> closecol;
    @FXML
    private TableColumn<?, ?> voolumecol;
    
	ObservableList<RealData> data1 = FXCollections.observableArrayList();
	@FXML
	private Label l�bel;
	
	  @FXML
	    private PieChart piechart;
	  @FXML
	    private LineChart<String, Number> linechart;

	  //  @FXML
	    //private LineChart<String, Number> linechart;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	
        // TODO
    }    

    @FXML
    private void CloseWindow(ActionEvent event) {
    }

    @FXML
    private void ForeignExchange(ActionEvent event) throws IOException {
    	
URL url= new URL("https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_INTRADAY&symbol=BTC&market=EUR&apikey=OPO1ZBRE9H3AFMMU");
  		
  		URLConnection urlconn=url.openConnection();
  		InputStreamReader inStream=new InputStreamReader(urlconn.getInputStream());
  		BufferedReader bff=new BufferedReader(inStream);
  		String line=bff.readLine();
  		while(line!=null){
  			
  			System.out.println(line);
  			line=bff.readLine();
  		}
    	
    }

    @FXML
    private void CurrencyExchangeRate(ActionEvent event) throws NamingException, IOException {
    	
URL url= new URL("https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=CNY&apikey=OPO1ZBRE9H3AFMMU");
  		
  		URLConnection urlconn=url.openConnection();
  		InputStreamReader inStream=new InputStreamReader(urlconn.getInputStream());
  		BufferedReader bff=new BufferedReader(inStream);
  		String line=bff.readLine();
  		while(line!=null){
  			
  			System.out.println(line);
  			line=bff.readLine();}
    	

    	
    }

    @FXML
    private void Stock(ActionEvent event) throws IOException, NamingException {
    	
    	String jndiName2 = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/RealDataRemote!tn.esprit.Finance_It_Team_server.services.IRealData";
		Context ctx2 = new InitialContext();
		IRealData proxy2 = (IRealData) ctx2.lookup(jndiName2);
		buildStocks();
		RemplirTable();
    }
    @FXML
    void acceuil(ActionEvent event) {

    }

    @FXML
    void archieve(ActionEvent event) {

    }

    @FXML
    void forum(ActionEvent event) {

    }

    @FXML
    void profile(ActionEvent event) {

    }

    @FXML
    void real(ActionEvent event) {

    }

    @FXML
    void statisticCliked(ActionEvent event) {

    }
    @FXML
    void CurrencyOptionClicked(ActionEvent event) {

    }

    @FXML
    void CalculateOptionClicked(ActionEvent event) {

    }
    @FXML
	void evenement(ActionEvent event) {

	}
    
    
private void buildStocks() throws NamingException{
		
		
		
		String jndiName2 = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/RealDataRemote!tn.esprit.Finance_It_Team_server.services.IRealData";
		Context ctx2 = new InitialContext();
		IRealData proxy2 = (IRealData) ctx2.lookup(jndiName2);

		
		
		String[] name = { "AAPL", "MSFT", "GOOG" };

		  

		for (int counter = 0; counter < name.length; counter++) {
			String response = HttpRequest.get("https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol="
					+ name[counter] + "&interval=1min&apikey=OPO1ZBRE9H3AFMMU").accept("application/json").body();
			
			System.out.println("response : " + response);
			
			JSONObject jsonObject = new JSONObject(response);

			Set<String> keysToFetchFrom = new HashSet<>();
			TreeSet<String> sortedKeysToFetchFrom = new TreeSet<>(new MyComp());
			keysToFetchFrom = ((JSONObject) jsonObject.get("Monthly Time Series")).keySet();
			sortedKeysToFetchFrom.addAll(keysToFetchFrom);
			String[] keys = new String[sortedKeysToFetchFrom.size() + 1];
			int index = 0;
			for (String K : sortedKeysToFetchFrom) {
				keys[index] = K;
				index++;
			}
			
			for (int i = 0; i < 1; i++) {
							JSONObject result = ((JSONObject) ((JSONObject) jsonObject.get("Monthly Time Series")).get(keys[1]));
							
				Float open = (float) (result.getDouble("1. open"));
				Float high = (float) (result.getDouble("2. high"));
				Float low = (float) (result.getDouble("3. low"));
				Float close = (float) (result.getDouble("4. close"));
				Long volume = (long) (result.getDouble("5. volume"));
				System.out.println(name[counter]);
				String name1=name[counter];
				RealData r1=new RealData(name1,open,high,low,close,volume);
				
				System.out.println("Symbols : " + name[counter] + " Open " + open + " high " + high + " low " + low + " close "+ close + " volume " + volume);
				proxy2.addRealData(r1);
				System.out.println(r1);
				
				ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(
			    		  new PieChart.Data("GOOG", open), 
			    	         new PieChart.Data("APPL", close), 
			    	         new PieChart.Data("MSFT", high));

				

						
				piechart.setData(pieChartData);
			} 
			piechart.getData().stream().forEach(data -> {
				data.getNode().addEventHandler(javafx.scene.input.MouseEvent.ANY, e -> {

					l�bel.setText("there are :  " + (int) data.getPieValue() + " unit exchange of " + data.getName());
				});
			});


		
			
			
			}
		}
		
			
				

				
		

		
		
		public void RemplirTable() throws NamingException {
			String jndiName2 = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/RealDataRemote!tn.esprit.Finance_It_Team_server.services.IRealData";
			Context ctx2 = new InitialContext();
			IRealData proxy2 = (IRealData) ctx2.lookup(jndiName2);

			namecol.setCellValueFactory(new PropertyValueFactory<>("name"));
			opencol.setCellValueFactory(new PropertyValueFactory<>("open"));
			highcol.setCellValueFactory(new PropertyValueFactory<>("high"));
			lowcol.setCellValueFactory(new PropertyValueFactory<>("low"));
			closecol.setCellValueFactory(new PropertyValueFactory<>("close"));
			voolumecol.setCellValueFactory(new PropertyValueFactory<>("volume"));
			RealData c = new RealData();
			List<RealData> o = proxy2.getAllrealdata();
			for (RealData e : o) {
				data1.add(e);
			}
			Tableview1.setItems(data1);
			

			}
		
	
			
		}


    

