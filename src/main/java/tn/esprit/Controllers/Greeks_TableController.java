package tn.esprit.Controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import tn.esprit.Finance_It_Team_server.services.CalculServiceRemote;
import tn.esprit.Finance_It_Team_server.services.GreeksRemote;
import javafx.event.ActionEvent;


public class Greeks_TableController implements Initializable {
	

    @FXML
    private JFXButton btnHome;

    @FXML
    private JFXButton btCurrencyOption;

    @FXML
    private JFXButton btProfile;

    @FXML
    private JFXButton btforum;

    @FXML
    private JFXButton btnCalcul;

    @FXML
    private Label hello_text;

    @FXML
    private Button btnClose;

    @FXML
    private AnchorPane holderPane;

    @FXML
    private Label DeltaCall_label;

    @FXML
    private Label VegaCall_label;

    @FXML
    private Label GammaCall_label;

    @FXML
    private Label RhoCall_label;

    @FXML
    private Label DeltaPut_label;

    @FXML
    private Label GammaPut_label;

    @FXML
    private Label VegaPut_label;

    @FXML
    private Label RhoPut_label;

    @FXML
    private Button Calculate_Price2;
    
    
    
    
    
    
    public void initialize(URL location, ResourceBundle resources) {
    	String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/Greeks!tn.esprit.Finance_It_Team_server.services.GreeksRemote";

		Context context = null;
		try {
			context = new InitialContext();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		try {
			GreeksRemote proxy = (GreeksRemote) context.lookup(jndiName);
			CalculatePriceController c1 = new CalculatePriceController();
			double currentprice=c1.getcurrentprice();
			double strikeprice=c1.getstrikeprice();
			double risk=c1.getrisk();
			double time=c1.gettime();
			double volatility=c1.getvolatility();
			double dividend=c1.getdividend();
			
			/********Delta Call********/
			double dc=proxy.delta(currentprice,strikeprice,risk,volatility,time,dividend,"call");
			String dcs = Double.toString(dc);
			DeltaCall_label.setText(dcs);
			double dp=proxy.delta(currentprice, strikeprice, risk, volatility, time, dividend, "put");
			String dps = Double.toString(dp);
			DeltaPut_label.setText(dps);
			
			
			
			double vc=proxy.vega(currentprice, strikeprice, risk, volatility, time,dividend);
			String vcs = Double.toString(vc);
			VegaCall_label.setText(vcs);
			VegaPut_label.setText(vcs);
			
			double gc=proxy.gamma(currentprice, strikeprice, risk, volatility, time, dividend);
			String gcs =Double.toString(gc);
			GammaCall_label.setText(gcs);
			GammaPut_label.setText(gcs);
			
			
			double rc=proxy.rho(currentprice, strikeprice, risk, volatility, time, "call");
			String rcs =Double.toString(rc);
			RhoCall_label.setText(rcs);
			
			double rp=proxy.rho(currentprice, strikeprice, risk, volatility, time, "put");
			String rps =Double.toString(rp);
			RhoPut_label.setText(rps);
			
			
			

		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		
		
		
		
		
		


	}
    

    @FXML
    void CalculateOptionClicked(ActionEvent event) {

    }

    @FXML
    void CloseWindow(ActionEvent event) {

    }

    @FXML
    void CurrencyOptionClicked(ActionEvent event) {

    }

    @FXML
    void acceuil(ActionEvent event) {

    }

    @FXML
    void forum(ActionEvent event) {

    }

    @FXML
    void price_click(ActionEvent event) {

    }

    @FXML
    void profile(ActionEvent event) {

    }

}
