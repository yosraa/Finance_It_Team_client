package tn.esprit.Controllers;

import com.jfoenix.controls.JFXButton;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.math.*;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import tn.esprit.Controllers.CalculatePriceController;
import tn.esprit.Finance_It_Team_server.services.CalculServiceRemote;
import javafx.scene.control.ComboBox;

public class ChartController implements Initializable {
	public void initialize(URL location, ResourceBundle resources) {

		combochart.setItems(graphs);

	}

	@FXML
	private Button GraphButton;

	@FXML
	private JFXButton btnHome;

	@FXML
	private JFXButton btCurrencyOption;

	@FXML
	private JFXButton btProfile;

	@FXML
	private JFXButton btforum;

	@FXML
	private JFXButton btnContacts;

	@FXML
	private Label hello_text;

	@FXML
	private Button btnClose;

	@FXML
	private AnchorPane holderPane;

	@FXML
	private Button addbutton;

	@FXML
	private Button reset;

	@FXML
	private Button delete;

	@FXML
	private LineChart<?, ?> LineChart;

	@FXML
	private CategoryAxis x;

	@FXML
	private NumberAxis y;
	@FXML
	private ComboBox<String> combochart;

	ObservableList<String> graphs = FXCollections.observableArrayList("long call", "short call", "long put",
			"short put");

	@FXML
	void CloseWindow(ActionEvent event) {

	}

	@FXML
	void CurrencyOptionClicked(ActionEvent event) {

	}

	@FXML
	void acceuil(ActionEvent event) {

	}

	@FXML
	void evenement(ActionEvent event) {

	}

	@FXML
	void forum(ActionEvent event) {

	}

	@FXML
	void onclickDelete(ActionEvent event) {

	}

	@FXML
	void profile(ActionEvent event) {

	}

	@FXML
	void reset(ActionEvent event) {

	}

	@FXML
	void onclicksubmit(ActionEvent event) {

		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/CalculService!tn.esprit.Finance_It_Team_server.services.CalculServiceRemote";

		Context context = null;
		try {
			context = new InitialContext();
		} catch (NamingException e) {

			e.printStackTrace();
		}
		CalculServiceRemote proxy = null;
		try {
			proxy = (CalculServiceRemote) context.lookup(jndiName);
		} catch (NamingException e) {

			e.printStackTrace();
		}

		String typegraph = combochart.getValue();

		CalculatePriceController c1 = new CalculatePriceController();

		double premium = c1.getopionvalue();
		double strike = c1.getstrikeprice();
		String type = c1.getoptiontype();

		XYChart.Series series = new XYChart.Series();

		double gain = 0;

		if (type.equalsIgnoreCase("call") && typegraph.equalsIgnoreCase("long call")) {
			for (int i = (int) strike - (int) (strike * 50); i <= strike + (int) (strike * 50); i = i
					+ (int) ((strike * 50))) {

				StringBuilder sbi = new StringBuilder();
				sbi.append("");
				sbi.append(i);
				String strIi = sbi.toString();

				gain = proxy.maxstk(i, strike) - premium;

				series.getData().add(new XYChart.Data(strIi, gain));
			}

		} else if (type.equalsIgnoreCase("put") && typegraph.equalsIgnoreCase("long put")) {
			for (int i = (int) strike - (int) (strike * 50); i <= strike + (int) (strike * 50); i = i
					+ (int) ((strike * 50))) {
				StringBuilder sbi = new StringBuilder();
				sbi.append("");
				sbi.append(i);
				String strIi = sbi.toString();

				gain = proxy.maxkst(strike, i) - premium;

				series.getData().add(new XYChart.Data(strIi, gain));
			}

		} else if (type.equalsIgnoreCase("call") && typegraph.equalsIgnoreCase("short call")) {
			for (int i = (int) strike - (int) (strike * 50); i <= strike + (int) (strike * 50); i = i
					+ (int) ((strike * 50))) {
				StringBuilder sbi = new StringBuilder();
				sbi.append("");
				sbi.append(i);
				String strIi = sbi.toString();

				gain = proxy.maxstk(i, strike) - premium;

				series.getData().add(new XYChart.Data(strIi, -gain));
			}

		} else if (type.equalsIgnoreCase("put") && typegraph.equalsIgnoreCase("short put")) {
			for (int i = (int) strike - (int) (strike * 50); i <= strike + (int) (strike * 50); i = i
					+ (int) ((strike * 50))) {
				StringBuilder sbi = new StringBuilder();
				sbi.append("");
				sbi.append(i);
				String strIi = sbi.toString();

				gain = proxy.maxkst(strike, i) - premium;

				series.getData().add(new XYChart.Data(strIi, -gain));
			}

		}

		LineChart.getData().addAll(series);

	}

}
