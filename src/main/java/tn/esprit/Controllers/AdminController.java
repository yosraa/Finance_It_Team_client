/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.Controllers;



import java.awt.Label;
import java.awt.MenuItem;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javafx.event.ActionEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import tn.esprit.Finance_It_Team_server.entities.StateOptionStock;
import tn.esprit.Finance_It_Team_server.entities.StockOption;
import tn.esprit.Finance_It_Team_server.entities.User;
import tn.esprit.Finance_It_Team_server.services.ServiceUser;
import tn.esprit.Finance_It_Team_server.services.IOptionProduct;
import tn.esprit.Finance_It_Team_server.services.IStockOptionService;
import tn.esprit.javafx.FXMLLoginController;
import javafx.scene.control.cell.PropertyValueFactory;


/**
 * FXML Controller class
 *
 * @author JEMMALI
 */
public class AdminController implements Initializable {
	   List<StockOption> arr = new ArrayList();

	    @FXML
	    private TextField rechercheOptionAdmin;

	    @FXML
	    private Label User_name;
    @FXML
    private Button deleteOption;
    @FXML
    private Button updateButton;
    @FXML
    private TableView<StockOption> tableViewOption;
	ObservableList<StockOption> data1 = FXCollections.observableArrayList();

    @FXML
    private TableColumn<StockOption, ?> id;
    @FXML
    private TableColumn<StockOption, ?> date;
    @FXML
    private TableColumn<StockOption, ?> status;
    @FXML
    private TableColumn<StockOption, ?> strikePrice;
    @FXML
    private TableColumn<StockOption, ?> type;
    @FXML
    private TableColumn<StockOption, ?> trader;
    @FXML
    private TableColumn<StockOption, ?> user;
    @FXML
    private TextField statusOption;
    @FXML
    private TableColumn<StockOption, ?> currentP;
    @FXML
    private TableColumn<StockOption, ?> risk;
    @FXML
    private TableColumn<StockOption, ?> nbshares;
    @FXML
    private MenuItem no;

    @FXML
    private MenuItem yes;
    public ObservableList<StockOption> list = FXCollections.observableArrayList();
    StockOption option = new StockOption();
    public static int i;

    /**
     * Initializes the controller class.
     */
    
    private void TableFlush (TableView<StockOption>  table ) {
    	for (int i = 0 ; i<= table.getItems().size() ; i ++ ){
    		table.getItems().clear();
    	}
    }
	public void initialize(URL location, ResourceBundle resources) {
		try {
			String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
			Context ctx = new InitialContext();
			IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);

			/*  if (FXMLLoginController.userRole==2) {*/
		          //  User_name.setText("Admin : " + );
			
	            
			
			
			List<StockOption> traders = proxy.getAllStockOption();
			//List<StockOption> traders = proxy.getChecked();

			id.setCellValueFactory(new PropertyValueFactory<>("id"));
			date.setCellValueFactory(new PropertyValueFactory<>("expirationDateStock"));
			currentP.setCellValueFactory(new PropertyValueFactory<>("currentPriceStock"));
			status.setCellValueFactory(new PropertyValueFactory<>("stateOptionStock"));
			strikePrice.setCellValueFactory(new PropertyValueFactory<>("strikePriceStock"));
			type.setCellValueFactory(new PropertyValueFactory<>("typeOptionStock"));
			risk.setCellValueFactory(new PropertyValueFactory<>("riskStock"));
			nbshares.setCellValueFactory(new PropertyValueFactory<>("nbShares"));
			trader.setCellValueFactory(new PropertyValueFactory<>("trader"));
			ObservableList<StockOption> items = FXCollections.observableArrayList(traders);

			tableViewOption.setItems(items);
			StockOption st = new StockOption();
			  
			List<StockOption> o = proxy.getAllStockOption();
			for (StockOption e : o) {
				data1.add(e);
			}
			rechercheOptionAdmin.textProperty().addListener((observable, oldValue, newValue) -> {
				Float v = new Float(newValue);
				data1.clear();
				data1.setAll(o.stream().filter(e -> e.getStrikePriceStock() >= v).collect(Collectors.toList()));
				tableViewOption.setItems(data1);
			});
			 // }

		} catch (NamingException e) {

			e.printStackTrace();
		}
	
		
	
	} 
	   @FXML
	    private void ActionUpdate(ActionEvent event) throws SQLException {
		//
	    }

	public void RemplirTable() throws SQLException, NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);
		StockOption c = new StockOption();

		id.setCellValueFactory(new PropertyValueFactory<>("id"));
		date.setCellValueFactory(new PropertyValueFactory<>("expirationDateStock"));
		currentP.setCellValueFactory(new PropertyValueFactory<>("currentPriceStock"));
		status.setCellValueFactory(new PropertyValueFactory<>("stateOptionStock"));
		strikePrice.setCellValueFactory(new PropertyValueFactory<>("strikePriceStock"));
		type.setCellValueFactory(new PropertyValueFactory<>("typeOptionStock"));
		risk.setCellValueFactory(new PropertyValueFactory<>("riskStock"));
		nbshares.setCellValueFactory(new PropertyValueFactory<>("nbShares"));
		trader.setCellValueFactory(new PropertyValueFactory<>("trader"));

		List<StockOption> o = proxy.getAllStockOption();
		for (StockOption e : o) {
			data1.add(e);
		}
		tableViewOption.setItems(data1);
	}
    @FXML
    private void ActionButtonDelete(ActionEvent event) throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);

		int idOption = tableViewOption.getSelectionModel().getSelectedItem().getId();
		proxy.deleteStockOption(idOption);
		List<StockOption> list = proxy.getAllStockOption();
		ObservableList<StockOption> items = FXCollections.observableArrayList(list);
		tableViewOption.setItems(items);
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Stock Option Removed");
		alert.setHeaderText("succesful");
		alert.showAndWait();
	}

 
    @FXML
    private void OnMouseClickedTable(MouseEvent event) {
    }
  
    @FXML
    void SetToNo(ActionEvent event) throws NamingException, SQLException {
    	String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);
		if (tableViewOption.getSelectionModel().getSelectedItem() != null) {
    	StockOption r = tableViewOption.getSelectionModel().getSelectedItem();
    	r.setStateOptionStock(StateOptionStock.no);
    	proxy.updateStockOption(r);		

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("State updated to NO");
		alert.setHeaderText("succesful");
		alert.showAndWait();
	
		TableFlush(tableViewOption);
		RemplirTable();
		}   }

    @FXML
    void SetToYes(ActionEvent event) throws NamingException, SQLException {
    	String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);
		if (tableViewOption.getSelectionModel().getSelectedItem() != null) {
    	StockOption r = tableViewOption.getSelectionModel().getSelectedItem();
    	r.setStateOptionStock(StateOptionStock.yes);
proxy.updateStockOption(r);		
Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("State updated to YES ");
		alert.setHeaderText("succesful");
		alert.showAndWait();
		/* 
		tableViewOption.getItems().clear();
		tableViewOption.refresh();
		tableViewOption.setItems(data1); */ 
		TableFlush(tableViewOption);

		RemplirTable();}    
		
		}
  
    
}

