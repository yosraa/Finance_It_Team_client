/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.Controllers;

import com.jfoenix.controls.JFXButton;
import javafx.scene.control.Alert;

import javafx.scene.control.TextField;
import javafx.scene.control.ButtonType;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import tn.esprit.Finance_It_Team_server.entities.StockOption;
import tn.esprit.Finance_It_Team_server.services.IOptionProduct;
import tn.esprit.Finance_It_Team_server.services.IStockOptionService;
import tn.esprit.javafx.FXMLLoginController;

/**
 * FXML Controller class
 *
 * @author JEMMALI
 */
public class EquityTradeOptionSHOWController implements Initializable {

	@FXML
	private Label evaluation;
	  @FXML
	    private Button stockbtn;
	@FXML
	private Button Evaluation;

	@FXML
	private TextField rechercheOptionT;
	@FXML
	private TextField recherche1;
	@FXML
	private JFXButton btnHome;
	@FXML
	private JFXButton btCurrencyOption;
	@FXML
	private JFXButton btProfile;
	@FXML
	private JFXButton btforum;
	@FXML
	private JFXButton btnContacts;

	@FXML
	private TextField sigma;
	@FXML
	private TextField SpotExchangeRate11;

	@FXML
	private TextField timeS;

	@FXML
	private TextField StrikeP;
	@FXML
	private TextField CurrentPr;
	@FXML
	private TextField riskFT;

	@FXML
	private LineChart<String, Number> LineChartDateOption;

	@FXML
	private Label price_label;
	@FXML
	private TextField id11;

	@FXML
	private Button calculateOption11;

	@FXML
	private Label price_label11;
	@FXML
	private Label hello_text;
	@FXML
	private Button btnClose;
	@FXML
	private AnchorPane holderPane;
	@FXML
	private Label sharesValue;
	@FXML
	private TableView<StockOption> TableStockOption;
	@FXML
	private TableColumn<StockOption, ?> id;
	@FXML
	private TableColumn<StockOption, ?> date;

	@FXML
	private TextField id1;

	@FXML
	private Button calculateOption1;

	@FXML
	private Label price_label1;
	@FXML
	private TableColumn<StockOption, ?> currentP;
	   @FXML
	    private Button logout;
	@FXML
	private TableColumn<StockOption, ?> status;

	@FXML
	private TableColumn<StockOption, ?> strikePrice;

	@FXML
	private Button calculateOption;

	@FXML
	private TableColumn<StockOption, ?> type;

	@FXML
	private TableColumn<StockOption, ?> risk;

	@FXML
	private TableColumn<StockOption, ?> nbshares;
	@FXML
	private TableView<StockOption> Tableview = new TableView<StockOption>();
	ObservableList<StockOption> data1 = FXCollections.observableArrayList();

	/**
	 * Initializes the controller class.
	 */
	public void initialize(URL url, ResourceBundle rb) {

		try {
			String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
			Context ctx = new InitialContext();
			IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);

		//	List<StockOption> traders = proxy.getAllStockOption();
			List<StockOption> traders = proxy.getAllStockOptionNotATrader(FXMLLoginController.userId);

			id.setCellValueFactory(new PropertyValueFactory<>("id"));

			date.setCellValueFactory(new PropertyValueFactory<>("expirationDateStock"));
			currentP.setCellValueFactory(new PropertyValueFactory<>("currentPriceStock"));
			status.setCellValueFactory(new PropertyValueFactory<>("stateOptionStock"));
			strikePrice.setCellValueFactory(new PropertyValueFactory<>("strikePriceStock"));
			type.setCellValueFactory(new PropertyValueFactory<>("typeOptionStock"));
			risk.setCellValueFactory(new PropertyValueFactory<>("riskStock"));
			nbshares.setCellValueFactory(new PropertyValueFactory<>("nbShares"));
			ObservableList<StockOption> items = FXCollections.observableArrayList(traders);
			TableStockOption.setItems(items);
			StockOption st = new StockOption();
			List<StockOption> o = proxy.getAllStockOptionNotATrader(FXMLLoginController.userId);

			//List<StockOption> o = proxy.getAllStockOption();
			for (StockOption e : o) {
				data1.add(e);
			}
			rechercheOptionT.textProperty().addListener((observable, oldValue, newValue) -> {
				System.out.println("hit");
				Float v = new Float(newValue);
				data1.clear();
				data1.setAll(o.stream().filter(e -> e.getStrikePriceStock() >= v).collect(Collectors.toList()));
				TableStockOption.setItems(data1);
			});

		} catch (NamingException e) {

			e.printStackTrace();
		}
	}

	@FXML
	private void acceuil(ActionEvent event) {
		//
	}

	@FXML
	private void CurrencyOptionClicked(ActionEvent event) {
		//
	}

	@FXML
	private void profile(ActionEvent event) {
		//
	}

	@FXML
	private void forum(ActionEvent event) {
		//
	}

	@FXML
	private void evenement(ActionEvent event) {
		//
	}

	@FXML
	private void CloseWindow(ActionEvent event) {
		//
	}

	@FXML
	private void mouse(MouseEvent event) {

		if (TableStockOption.getSelectionModel().getSelectedItem() != null) {
			StockOption r = TableStockOption.getSelectionModel().getSelectedItem();

			StrikeP.setText(r.getStrikePriceStock() + "");
			CurrentPr.setText(r.getCurrentPriceStock() + "");
			riskFT.setText(r.getRiskStock() + "");
			timeS.setText(r.getTimeStock() + "");
			id1.setText(r.getId() + "");
			id11.setText(r.getId() + "");
		}

	}

	@FXML
	void calculateOptionWarrant(ActionEvent event) throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);

		boolean empty = id1.getText().equals("");
		if (empty == true) {
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Empty or Incorrect Field ");
			alert.setHeaderText("Complete All fields ");
			Optional<ButtonType> result = alert.showAndWait();
			return;
		}

		Integer a = Integer.parseInt(id1.getText());
		Float prix = proxy.WarrantPriceAdjustedBS(a);
		String pri = Float.toString(prix);
		price_label1.setText(pri);
	}

	@FXML
	void calculateOption(ActionEvent event) throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);
		boolean empty = (StrikeP.getText().equals("") || CurrentPr.getText().equals("") || riskFT.getText().equals("")
				|| timeS.getText().equals("") || sigma.getText().equals("") || SpotExchangeRate11.getText().equals(""));
		if (empty == true) {
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Empty or Incorrect Field ");
			alert.setHeaderText("Complete All fields ");
			Optional<ButtonType> result = alert.showAndWait();
			return;
		}
		Float a = Float.parseFloat(StrikeP.getText());
		Float b = Float.parseFloat(CurrentPr.getText());
		Float c = Float.parseFloat(riskFT.getText());
		Float d = Float.parseFloat(timeS.getText());
		Float e = Float.parseFloat(sigma.getText());
		Float f = Float.parseFloat(SpotExchangeRate11.getText());

		double prix = proxy.PriceOfALookbackOption(a, b, c, d, e, f);
		String pri = Double.toString(prix);
		price_label.setText(pri);

	}
	  @FXML
	    void onclickStock(ActionEvent event) {
	    	try {

				Stage stage = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/YahooFinanceHistoricalDataMalek/HomeManagement.fxml"));
				Scene scene = new Scene(root);
				stage.setScene(scene);
				stage.show();

			} catch (IOException ex) {
				Logger.getLogger(ShowStockOptionController.class.getName()).log(Level.SEVERE, null, ex);
			}
	    }
	@FXML
	void evaluation(ActionEvent event) throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);
		
		Integer a = Integer.parseInt(id11.getText());
		String eval = proxy.DeepInOutAtTheMoney(a);
		evaluation.setText(eval);
		}

	@FXML
	void calculateOptionContract(ActionEvent event) throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/StockOptionServiceRemote!tn.esprit.Finance_It_Team_server.services.IStockOptionService";
		Context ctx = new InitialContext();
		IStockOptionService proxy = (IStockOptionService) ctx.lookup(jndiName);

		boolean empty = id11.getText().equals("");
		if (empty == true) {
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Empty or Incorrect Field ");
			alert.setHeaderText("Complete All fields ");
			Optional<ButtonType> result = alert.showAndWait();
			return;
		}

		Integer a = Integer.parseInt(id11.getText());
		Float prix = proxy.futuresPrice(a);
		String pri = Float.toString(prix);
		price_label11.setText(pri);
	}

	@FXML
    void logout(ActionEvent event)throws NamingException, IOException {
    	Stage stage = (Stage) logout.getScene().getWindow();
		stage.close();
    }
}
