/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.Controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;

import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.Name;
import javax.naming.NameClassPair;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.persistence.Id;

import com.sun.glass.ui.Window.Level;
import com.sun.javafx.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.github.kevinsawicki.http.HttpRequest;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import tn.esprit.Finance_It_Team_server.entities.CurrencyAccount;
import tn.esprit.Finance_It_Team_server.entities.TypeCurrency;
import tn.esprit.Finance_It_Team_server.services.ICurrencyAccount;
import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.Portfolio;
import tn.esprit.Finance_It_Team_server.entities.RealData;
import tn.esprit.Finance_It_Team_server.entities.StateOption;
import tn.esprit.Finance_It_Team_server.entities.TypeOption;
import tn.esprit.Finance_It_Team_server.entities.UnitExchange;
import tn.esprit.Finance_It_Team_server.services.IOptionProduct;
import tn.esprit.Finance_It_Team_server.services.IRealData;
import tn.esprit.javafx.FXMLLoginController;
import tn.esprit.javafx.Main;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * FXML Controller class
 *
 * @author Amine
 */
public class CurrencyAccountController implements Initializable {

	private CurrencyAccount scarte = new CurrencyAccount();

	@FXML
	private Label hello_text;
	@FXML
	private Button btnClose;
	@FXML
	private AnchorPane holderPane;
	@FXML
	private TextField Amountentry;
	@FXML
	private TableView<CurrencyAccount> Tableview = new TableView<CurrencyAccount>();

	@FXML
	private TextField username;

	@FXML
	private Button addbutton;

	@FXML
	private Button Updatebutton11;
	@FXML
	private Button searchBtn;
	@FXML
	private Button removebutton1;
	@FXML
	private TableColumn<?, ?> Identitycol;
	@FXML
	private TableColumn<?, ?> Amountcol;
	@FXML
	private TableColumn<?, ?> Incimescol;
	@FXML
	private TableColumn<?, ?> Expensescol;
	@FXML
	private TableColumn<?, ?> Typecol;

	private ObservableList<TypeCurrency> Currency = FXCollections.observableArrayList(TypeCurrency.dollar,
			TypeCurrency.euro, TypeCurrency.pound, TypeCurrency.yen);
	@FXML
	private ComboBox<TypeCurrency> CurrencyTypeentry;

	ObservableList<CurrencyAccount> data1 = FXCollections.observableArrayList();

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {

		CurrencyTypeentry.setItems(Currency);

		try {
			//buildStocks();
			RemplirTable();
			Tableview.getSelectionModel().selectedItemProperty()
					.addListener(((observable, oldValue, newValue) -> showdetails(newValue)));

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void CloseWindow(ActionEvent event) {
		Stage stage = (Stage) btnClose.getScene().getWindow();
		stage.close();

	}

	@FXML
	private void Submit(ActionEvent event) throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/CurrencyAccountRemote!tn.esprit.Finance_It_Team_server.services.ICurrencyAccount";
		Context ctx = new InitialContext();
		ICurrencyAccount proxy = (ICurrencyAccount) ctx.lookup(jndiName);

		CurrencyAccount account = new CurrencyAccount();
		account.setAmount(Float.parseFloat(Amountentry.getText()));
		account.setTypecurrency(CurrencyTypeentry.getValue());
		Tableview.getItems().clear();
		if (Amountentry.getText().equals("0")) {
			Alert alert = new Alert(Alert.AlertType.WARNING);
			alert.setTitle("Attention !");
			alert.setHeaderText("Vous Ne Pouvez Pas Ajouter Un Compte Avec Un Montant Null !");
			alert.setContentText("");

			Optional<ButtonType> result = alert.showAndWait();
			RemplirTable();

		} else {

			proxy.addCurrecnyAccount(account,FXMLLoginController.userId);
			RemplirTable();
			Amountentry.clear();
			CurrencyTypeentry.getSelectionModel().clearSelection();
			System.out.println(account);
		}

	}

	@FXML
	private void Update(ActionEvent event) throws NamingException {

		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/CurrencyAccountRemote!tn.esprit.Finance_It_Team_server.services.ICurrencyAccount";
		Context ctx = new InitialContext();
		ICurrencyAccount proxy = (ICurrencyAccount) ctx.lookup(jndiName);
		if (Tableview.getSelectionModel().getSelectedItem() != null) {
			CurrencyAccount r = Tableview.getSelectionModel().getSelectedItem();
			r.setAmount(Float.parseFloat(Amountentry.getText()));
			r.setTypecurrency(CurrencyTypeentry.getValue());

			proxy.updateCurrecnyAccount(r);
			Tableview.getItems().clear();

			try {
				RemplirTable();
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	@FXML
	private void Remove(ActionEvent event) throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/CurrencyAccountRemote!tn.esprit.Finance_It_Team_server.services.ICurrencyAccount";
		Context ctx = new InitialContext();
		ICurrencyAccount proxy = (ICurrencyAccount) ctx.lookup(jndiName);

		proxy.deleteCurrecnyAccount(Tableview.getSelectionModel().getSelectedItem().getId());
		Tableview.getItems().clear();

		try {
			RemplirTable();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//

	}

	@FXML
	void CurrencyOptionClicked(ActionEvent event) {

	}

	@FXML
	void acceuil(ActionEvent event) {

	}

	@FXML
	void evenement(ActionEvent event) {

	}

	@FXML
	void forum(ActionEvent event) {

	}

	@FXML
	void profile(ActionEvent event) {

	}

	public void RemplirTable() throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/CurrencyAccountRemote!tn.esprit.Finance_It_Team_server.services.ICurrencyAccount";
		Context ctx = new InitialContext();
		ICurrencyAccount proxy = (ICurrencyAccount) ctx.lookup(jndiName);

		Identitycol.setCellValueFactory(new PropertyValueFactory<>("id"));
		Amountcol.setCellValueFactory(new PropertyValueFactory<>("amount"));
		Typecol.setCellValueFactory(new PropertyValueFactory<>("typecurrency"));
		Incimescol.setCellValueFactory(new PropertyValueFactory<>("sumIncome"));
		Expensescol.setCellValueFactory(new PropertyValueFactory<>("sumExpenses"));

		CurrencyAccount c = new CurrencyAccount();
		List<CurrencyAccount> o = proxy.getAllCurrencyAccount();
		for (CurrencyAccount e : o) {
			data1.add(e);
		}

		Tableview.setItems(data1);
		// setPredicate((Predicate<? super CurrencyAccount>) CurrencyAccount ->;
		FilteredList<CurrencyAccount> filteredData = new FilteredList<>(data1, e -> true);
		username.setOnKeyReleased(e -> {
			username.textProperty().addListener((observable, oldValue, newValue) -> {
				filteredData.setPredicate((Predicate<? super CurrencyAccount>) CurrencyAccount -> {
					if (newValue == null || newValue.isEmpty()) {
						return true;
					}
					String lowerCaseFilter = newValue.toLowerCase();
					/*
					 * if (colocation.getNbChambre().contains(newValue)){ return
					 * true; }
					 */
					if ((String.valueOf(CurrencyAccount.getAmount()).toLowerCase()
							.contains(CharSequence.class.cast(lowerCaseFilter)))) {
						return true;
					}
					return false;

				});
			});
			SortedList<CurrencyAccount> sortedData = new SortedList<>(filteredData);
			sortedData.comparatorProperty().bind(Tableview.comparatorProperty());
			Tableview.setItems(sortedData);

		});

	}

	void showdetails(CurrencyAccount ca) {
		String idnew = Integer.toString(ca.getId());

		Amountentry.setText(Amountentry.getText().toString());
		CurrencyTypeentry.setValue(ca.getTypecurrency());

	}

	@FXML
	void reset(ActionEvent event) {
		Amountentry.clear();
		CurrencyTypeentry.getSelectionModel().clearSelection();

	}

	
	
	
	
	

	}


