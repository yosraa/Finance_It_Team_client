package tn.esprit.Controllers;


import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import tn.esprit.Finance_It_Team_server.entities.CurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.StateOption;
import tn.esprit.Finance_It_Team_server.services.IOptionProduct;


public class AdminCurrencyOptionController implements Initializable {

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			RemplirTable();
		} catch (SQLException e) {
			Logger.getLogger(CurrencyOptionController.class.getName()).log(Level.SEVERE, null, e);
			e.printStackTrace();
		} catch (NamingException e) {

			e.printStackTrace();
		}

	}

	@FXML
	private JFXButton btnHome;

	@FXML
	private JFXButton btCurrencyOption;

	@FXML
	private JFXButton btProfile;

	@FXML
	private JFXButton btforum;

	@FXML
	private JFXButton btnContacts;

	@FXML
	private Label hello_text;

	@FXML
	private Button btnClose;

	@FXML
	private AnchorPane holderPane;

	@FXML
	private Button addbutton;

	@FXML
	private Button reset;



	@FXML
	private JFXTextField searchFT;

	@FXML
	private Button searchBtn;

	@FXML
	private TableView<CurrencyOption> TableCurrencyOptionAD;
	ObservableList<CurrencyOption> data1 = FXCollections.observableArrayList();
	@FXML
	private TableColumn<?, ?> StrikePriceColumn;

	@FXML
	private TableColumn<?, ?> CurrentPriceColumn;

	@FXML
	private TableColumn<?, ?> TypeOptionColumn;

	@FXML
	private TableColumn<?, ?> volatilityColumn;

	@FXML
	private TableColumn<?, ?> RiskColumn;

	@FXML
	private TableColumn<?, ?> TimeColumn;

	@FXML
	private TableColumn<?, ?> UnitExchangeColumn;

	@FXML
	private TableColumn<?, ?> RateColumn;

	@FXML
	private TableColumn<?, ?> StateCurrencyColumn;

	@FXML
	private TableColumn<?, ?> ExpirationDateColumn;

	@FXML
	void CloseWindow(ActionEvent event) {

	}

	@FXML
	void CurrencyOptionClicked(ActionEvent event) {

	}

	@FXML
	void acceuil(ActionEvent event) {

	}

	@FXML
	void evenement(ActionEvent event) {

	}

	@FXML
	void forum(ActionEvent event) {

	}


	@FXML
	void onclicksubmit(ActionEvent event) throws NamingException, SQLException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";
		Context ctx = new InitialContext();
		IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);
		CurrencyOption c = TableCurrencyOptionAD.getSelectionModel().getSelectedItem();
		if (TableCurrencyOptionAD.getSelectionModel().getSelectedItem().getStateOption() == StateOption.unavailable) {

			c.setStateOption(StateOption.available);
			proxy.updateOption(c);

			RemplirTable();

		}

	}

	@FXML
	void profile(ActionEvent event) {

	}

	@FXML
	void reset(ActionEvent event) {
		  Stage stage = (Stage) reset.getScene().getWindow();
				stage.close();
	}

	@FXML
	void searchClick(ActionEvent event) {

	}

	@FXML
	void selectItem() {

	}

	public void RemplirTable() throws SQLException, NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";
		Context ctx = new InitialContext();
		IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);
		CurrentPriceColumn.setCellValueFactory(new PropertyValueFactory<>("currentPrice"));
		TypeOptionColumn.setCellValueFactory(new PropertyValueFactory<>("Typeoption"));
		ExpirationDateColumn.setCellValueFactory(new PropertyValueFactory<>("expirationDate"));
		RiskColumn.setCellValueFactory(new PropertyValueFactory<>("risk"));
		RateColumn.setCellValueFactory(new PropertyValueFactory<>("spotExchangeRate"));
		StateCurrencyColumn.setCellValueFactory(new PropertyValueFactory<>("StateOption"));
		StrikePriceColumn.setCellValueFactory(new PropertyValueFactory<>("strikePrice"));
		TimeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));
		UnitExchangeColumn.setCellValueFactory(new PropertyValueFactory<>("UnitEchange"));
		volatilityColumn.setCellValueFactory(new PropertyValueFactory<>("volatility"));

		List<CurrencyOption> o = proxy.getAllCurrencyOption1();
		for (CurrencyOption e : o) {
			data1.add(e);
		}
		TableCurrencyOptionAD.setItems(data1);
	}

}