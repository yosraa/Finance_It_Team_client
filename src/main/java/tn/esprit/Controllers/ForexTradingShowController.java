package tn.esprit.Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import tn.esprit.Finance_It_Team_server.entities.AchatCurrencyOption;
import tn.esprit.Finance_It_Team_server.entities.CurrencyAccount;
import tn.esprit.Finance_It_Team_server.entities.StateTrade;
import tn.esprit.Finance_It_Team_server.services.AchatCurrencyOptionRemote;
import tn.esprit.javafx.FXMLLoginController;

public class ForexTradingShowController implements Initializable {
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");

		String datee = dateFormat.format(CurrencyOptionController.dateExpiration);
		TFexpirationDate.setText(datee);
		try {
			RemplirTable2();
		} catch (SQLException | NamingException e) {

			e.printStackTrace();
		}

	}
    
	@FXML
	private JFXButton btnHome;

	@FXML
	private JFXButton btCurrencyOption;

	@FXML
	private JFXButton btProfile;

	@FXML
	private JFXButton btforum;

	@FXML
	private JFXButton btnContacts;

	@FXML
	private Label hello_text;

	@FXML
	private Button btnClose;

	@FXML
	private AnchorPane holderPane;

	@FXML
	private JFXTextField searchFT;

	@FXML
	private Button searchBtn;
	 @FXML
	 private Button goBack;

	@FXML
	private TableView<AchatCurrencyOption> TableTrade;

	@FXML
	private TableColumn<AchatCurrencyOption, ?> SettDateColumn;

	@FXML
	private TableColumn<AchatCurrencyOption, ?> StadeTradeColumn;
    //@FXML
    //private TableColumn<AchatCurrencyOption, ?> PurchaseColumn;
    @FXML
    private TextField TFpurchase;

	@FXML
	private TextField TFexpirationDate;

	@FXML
	private TextField TFnameTrader;

	@FXML
	private TextField TFtypeForexOption;
	@FXML
	private Button BtnOK;
	@FXML
	private DatePicker PickerSettelment;
	@FXML
	private TextField FTidCurrencyOptionSelected;
    @FXML
    private Label TradeRefused;
    @FXML
    private Label TradeAccepted;
    @FXML
    private Label TradePending;
    @FXML
    private ImageView refused1;
    @FXML
    private ImageView accepted1;

    @FXML
    private ImageView pending1;

	private ObservableList<AchatCurrencyOption> dataa = FXCollections.observableArrayList();

	@FXML
	void CloseWindow(ActionEvent event) {

	}

	@FXML
	void CurrencyOptionClicked(ActionEvent event) {

	}

	@FXML
	void acceuil(ActionEvent event) {

	}
	 @FXML
	    void back(ActionEvent event) {
		  Stage stage = (Stage) goBack.getScene().getWindow();
			stage.close();
	    }

	@FXML
	void evenement(ActionEvent event) {

	}

	@FXML
	void OnClickdOk(ActionEvent event) throws NamingException, SQLException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/AchatCurrencyOptionService!tn.esprit.Finance_It_Team_server.services.AchatCurrencyOptionRemote";
		Context ctx1 = new InitialContext();
		AchatCurrencyOptionRemote proxy = (AchatCurrencyOptionRemote) ctx1.lookup(jndiName);

		AchatCurrencyOption achatCurrencyOption = new AchatCurrencyOption();
		LocalDate a = PickerSettelment.getValue();
		java.sql.Date dateSettelment = java.sql.Date.valueOf(a);
		// il faut recuperer l id de l'option selectionner et l'affecter dans la
		// fct ajout de la porteuse
		int idcu = CurrencyOptionController.idCurrencyOption;
		float strikep=CurrencyOptionController.strikePrice;
		String purchase1=TFpurchase.getText();
		int idtrader = FXMLLoginController.userId;
		int idtraderSell=CurrencyOptionController.idTraderSell;
		Date dateE=CurrencyOptionController.dateExpiration;
        

		System.out.println(idcu);
		System.out.println(idtrader);
		int settle=dateE.getDay();
		System.out.println(dateE);
		System.out.println(settle);

		 float amount0= proxy.getAmountFromCurrencyAccount();
		 float amount10= proxy.getAmountFromCurrencyAccount12();
		if(strikep<amount0 ){
	 	
		proxy.ajouterAchatCurrencyOption(idtrader, idcu,purchase1, dateSettelment, StateTrade.Accepted);
	   float amount1=amount0-strikep;
	   float amount2=amount10+strikep;
	   proxy.setAmount(proxy.findtrader(idtrader),amount1);
	   proxy.setAmount(proxy.findtrader(idtraderSell),amount2);
       TradeAccepted.setVisible(true);
       accepted1.setVisible(true);
     	
		
		}
		
		else if(strikep==amount0){proxy.ajouterAchatCurrencyOption(idtrader, idcu,purchase1, dateSettelment, StateTrade.Pending);
		
		TradePending.setVisible(true);
		pending1.setVisible(true);}
		else {   
		TradeRefused.setVisible(true);
		refused1.setVisible(true);
		
		}
	}

	@FXML
	void forum(ActionEvent event) {

	}

	@FXML
	void profile(ActionEvent event) {

	}

	@FXML
	void searchClick(ActionEvent event) {

	}

	@FXML
	void selectItem(MouseEvent event) {

		if (TableTrade.getSelectionModel().getSelectedItem() != null) {
			AchatCurrencyOption achat = TableTrade.getSelectionModel().getSelectedItem();

			DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");

			String datee = dateFormat.format(achat.getCurrencyOption().getExpirationDate());
			TFexpirationDate.setText(datee);
			TFnameTrader.setText(achat.getTrader().getFirstName());

			String name = achat.getCurrencyOption().getTypeoption().name();

			TFtypeForexOption.setText(name);
		}
	}

	public void RemplirTable2() throws SQLException, NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/AchatCurrencyOptionService!tn.esprit.Finance_It_Team_server.services.AchatCurrencyOptionRemote";
		Context ctx1 = new InitialContext();
		AchatCurrencyOptionRemote proxy = (AchatCurrencyOptionRemote) ctx1.lookup(jndiName);
		SettDateColumn.setCellValueFactory(new PropertyValueFactory<>("settelmentDate"));
		StadeTradeColumn.setCellValueFactory(new PropertyValueFactory<>("stateTrade"));
		//PurchaseColumn.setCellValueFactory(new PropertyValueFactory<>("nom"));
		List<AchatCurrencyOption> achatCurrencyOption = proxy.getAllTrade();
		for (AchatCurrencyOption a : achatCurrencyOption) {

			dataa.add(a);
		}
		TableTrade.setItems(dataa);
	}

}
