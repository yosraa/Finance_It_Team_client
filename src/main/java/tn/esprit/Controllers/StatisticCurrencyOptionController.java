package tn.esprit.Controllers;

import java.net.URL;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.AnchorPane;
import tn.esprit.Finance_It_Team_server.services.IOptionProduct;

public class StatisticCurrencyOptionController implements Initializable {
	@Override
	public void initialize(URL location, ResourceBundle resources) {

	
			try {
				MakeLineGraph();
			} catch (NamingException e) {

				e.printStackTrace();
			}
		
			try {
				MakeLineGraph2();
			} catch (NamingException e) {

				e.printStackTrace();
			}
		
			try {
				MakePieChartGraph3();
			} catch (NamingException e) {

				e.printStackTrace();
			}
			try {
				MakeProgressBarGraph4();
			} catch (NamingException e) {

				e.printStackTrace();
			}			
		
	}
	 @FXML
	    private ProgressBar progressBar;

	    @FXML
	    private ProgressIndicator progressIndicat;
	    @FXML
	    private ProgressBar progressBar1;

	    @FXML
	    private ProgressIndicator progressIndicat1;
	@FXML
	private JFXButton btnHome;

	@FXML
	private JFXButton btCurrencyOption;

	@FXML
	private JFXButton btProfile;

	@FXML
	private JFXButton btforum;

	@FXML
	private JFXButton btnContacts;

	@FXML
	private Label hello_text;

	@FXML
	private Button btnClose;

	@FXML
	private AnchorPane holderPane;

	@FXML
	private PieChart PieChartStat;
	@FXML
	private Label l�bel;

	@FXML
	private LineChart<String, Number> LineChartDate;

	@FXML
	private LineChart<String, Number> LineChartput;

	@FXML
	void CloseWindow(ActionEvent event) {

	}

	@FXML
	void CurrencyOptionClicked(ActionEvent event) {

	}

	@FXML
	void acceuil(ActionEvent event) {

	}

	@FXML
	void evenement(ActionEvent event) {

	}

	@FXML
	void forum(ActionEvent event) {

	}

	@FXML
	void profile(ActionEvent event) {

	}

	@SuppressWarnings("unchecked")
	private void MakeLineGraph() throws NamingException {
		LineChartDate.setTitle(" Price call Visualization per Month ");
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";

		Context ctx = new InitialContext();
		IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);
		XYChart.Series<String, Number> series = new XYChart.Series<>();
		XYChart.Series<String, Number> series1 = new XYChart.Series<>();

		List<Object[]> list = proxy.StrikePriceOfThisMonth1();
		List<Object[]> list1 = proxy.CurrentPriceOfThisMonth1();

		String PATTERN = "yyyy-MM-dd";
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		dateFormat.applyPattern(PATTERN);
		String today = dateFormat.format(Calendar.getInstance().getTime());
		int moisNow = Integer.parseInt((today.toString().substring(5, 7)));

		for (Object[] o : list) {

			Double price = (Double) o[0];

			Date date = (Date) o[1];

			String date1 = dateFormat.format(date.getTime());

			int mois = Integer.parseInt((date1.toString().substring(5, 7)));

			if (mois == moisNow) {

				series.getData().add(new XYChart.Data<>(date.toString(), price));

			}
			for (Object[] o1 : list1) {

				Double price1 = (Double) o1[0];

				Date date0 = (Date) o1[1];

				String date10 = dateFormat.format(date0.getTime());

				int mois1 = Integer.parseInt((date10.toString().substring(5, 7)));

				if (mois1 == moisNow) {

					series1.getData().add(new XYChart.Data<>(date0.toString(), price1));

				}

		}

		series.setName("Strike price call");
		series1.setName("Current price call");
		LineChartDate.getData().addAll(series,series1);}

	}

	@SuppressWarnings("unchecked")
	private void MakeLineGraph2() throws NamingException {
		LineChartput.setTitle("Price put Visualization per Month ");
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";

		Context ctx = new InitialContext();
		IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);
		XYChart.Series<String, Number> series2 = new XYChart.Series<>();
		XYChart.Series<String, Number> series3 = new XYChart.Series<>();
		List<Object[]> list2 = proxy.StrikePriceOfThisMonth2();
		List<Object[]> list3 = proxy.CurrentPriceOfThisMonth2();
		String PATTERN = "yyyy-MM-dd";
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		dateFormat.applyPattern(PATTERN);
		String today = dateFormat.format(Calendar.getInstance().getTime());
		int moisNow = Integer.parseInt((today.toString().substring(5, 7)));

		for (Object[] o2 : list2) {

			Double price = (Double) o2[0];

			Date date1 = (Date) o2[1];

			String date12 = dateFormat.format(date1.getTime());

			int mois2 = Integer.parseInt((date12.toString().substring(5, 7)));

			if (mois2 == moisNow) {

				series2.getData().add(new XYChart.Data<>(date1.toString(), price));

			}
			for (Object[] o3 : list3) {

				Double price3 = (Double) o3[0];

				Date date3 = (Date) o3[1];

				String date13 = dateFormat.format(date3.getTime());

				int mois3 = Integer.parseInt((date13.toString().substring(5, 7)));

				if (mois3 == moisNow) {

					series3.getData().add(new XYChart.Data<>(date3.toString(), price3));

				}
		}
        
		
		series2.setName("Strike price put");
		series3.setName("Current price put");
		
		LineChartput.getData().addAll(series2,series3);}

	}

	private void MakePieChartGraph3() throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";
		try {
			Context ctx = new InitialContext();
			IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);

			ObservableList<PieChart.Data> pieChartData = null;
			pieChartData = FXCollections.observableArrayList(
					new PieChart.Data("First Type: Eur_TND ", proxy.calculerUnitEur_TND()),
					new PieChart.Data("second Type: JPY_EUR", proxy.calculerUnitJPY_EUR()),
					new PieChart.Data("Third Type: USD_EUR", proxy.calculerUnitUSD_EUR()),
					new PieChart.Data("Forth Type: CAD_EUR ", proxy.calculerUnitCAD_EUR()),
					new PieChart.Data("Fifth Type: USD_TDN ", proxy.calculerUnitUSD_TDN()));

			PieChartStat.setData(pieChartData);
		} catch (NamingException e) {

			e.printStackTrace();
		}
		PieChartStat.getData().stream().forEach(data -> {
			data.getNode().addEventHandler(javafx.scene.input.MouseEvent.ANY, e -> {

				l�bel.setText("there are :  " + (int) data.getPieValue() + " unit exchange of " + data.getName());
			});
		});
	}
	
	private void MakeProgressBarGraph4() throws NamingException {
		String jndiName = "Finance_It_Team_server-ear/Finance_It_Team_server-ejb/OptionProductServiceRemote!tn.esprit.Finance_It_Team_server.services.IOptionProduct";

		Context ctx = new InitialContext();
		IOptionProduct proxy = (IOptionProduct) ctx.lookup(jndiName);

		long c = proxy.countRisk();
		float c1 = (long) ((float) c);
		long mc = proxy.countRiskMax();
		float mc1 = (long) ((float) mc);
		float pourcentage = mc1 / c1;

		float c2 = (long) ((float) c1);
		long mc2 = proxy.countRiskMin();
		float mc22 = (long) ((float) mc2);
		float pourcentage1 = mc22 / c2;

		progressBar.setProgress(pourcentage);
		progressIndicat.setProgress(pourcentage);
		progressBar.setStyle("-fx-accent: red;"); // line (1)
		progressIndicat.setStyle("-fx-accent: red;"); // line (2)

		progressIndicat1.setProgress(pourcentage1);
		progressBar1.setProgress(pourcentage1);
		progressBar1.setStyle("-fx-accent: green;"); // line (1)
		progressIndicat1.setStyle("-fx-accent: green;"); // line (2)

		

}}