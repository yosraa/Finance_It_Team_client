package YahooFinanceHistoricalDataMalek;

import java.io.File;
import java.io.FileNotFoundException;
//https://mvnrepository.com/artifact/com.yahoofinance-api/YahooFinanceAPI/3.12.3
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;

public class yahooTestStocks {

	public static void main(String[] args) throws IOException {

		Stock goog = YahooFinance.get("GOOG");
		goog.print();
		List<HistoricalQuote> listehq = new ArrayList<HistoricalQuote>();
		listehq = goog.getHistory();

		for (HistoricalQuote historicalQuote : listehq) {

			System.out.println("symbol " + historicalQuote.getSymbol());
			System.out.println("adj close" + historicalQuote.getAdjClose());
			System.out.println("close " + historicalQuote.getClose());
			System.out.println("date " + historicalQuote.getDate().getTime());
			System.out.println("high" + historicalQuote.getHigh());
			System.out.println("low " + historicalQuote.getLow());
			System.out.println("open " + historicalQuote.getOpen());
			System.out.println("volume " + historicalQuote.getVolume());

		}

	}

}

/*
 * package YahooFinanceHistoricalDataMalek;
 * 
 * import java.io.File; import java.io.FileNotFoundException;
 * //https://mvnrepository.com/artifact/com.yahoofinance-api/YahooFinanceAPI/3.
 * 12.3 import java.io.IOException; import java.io.PrintStream; import
 * java.util.ArrayList; import java.util.Iterator; import java.util.List; import
 * java.util.Scanner;
 * 
 * import yahoofinance.Stock; import yahoofinance.YahooFinance; import
 * yahoofinance.histquotes.HistoricalQuote;
 * 
 * 
 * public class yahooTestStocks {
 * 
 * public static void main(String[] args) throws IOException {
 * 
 * try { PrintStream myconsole =new PrintStream(new File("D://java.txt"));
 * System.setOut(myconsole);
 * 
 * Stock goog = YahooFinance.get("GOOG"); goog.print();
 * List<HistoricalQuote>listehq=new ArrayList<HistoricalQuote>();
 * listehq=goog.getHistory(); myconsole.print(" aaaaa: "+listehq+"\n");
 * 
 * 
 * //https://www.youtube.com/watch?v=72BEuCPMgTo for (HistoricalQuote
 * historicalQuote : listehq) {
 * 
 * 
 * 
 * myconsole.print(" symbol: "+historicalQuote.getSymbol()+"\n");
 * myconsole.print(" adj close:"+historicalQuote.getAdjClose()+"\n");
 * myconsole.print(" close: " +historicalQuote.getClose()+"\n");
 * myconsole.print(" date: "+historicalQuote.getDate().getTime()+"\n");
 * myconsole.print(" high: "+historicalQuote.getHigh()+"\n"); myconsole.print(
 * " low: "+historicalQuote.getLow()+"\n"); myconsole.print(" open: "
 * +historicalQuote.getOpen()+"\n"); myconsole.print(" volume: "
 * +historicalQuote.getVolume()+"\n");
 * 
 * 
 * myconsole.print("\n");
 * 
 * }
 * 
 * } catch (FileNotFoundException fx) { System.out.println(fx); }
 * 
 * 
 * }
 * 
 * }
 * 
 */
