package YahooFinanceHistoricalDataMalek;
/*

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import javafx.scene.Node;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.stage.Stage;
import YahooFinanceHistoricalDataMalek.Stocks;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Tooltip;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.jfoenix.controls.JFXTextField;

import YahooFinanceHistoricalDataMalek.HomeManagementController;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

public class StockChart extends Application {

	private static final int MAX_DATA_POINTS = 50;
	private int xSeriesData = 0;
	private XYChart.Series series2;
	private XYChart.Series series3;
	private ExecutorService executor;
	private AddToQueue addToQueue;
	private ConcurrentLinkedQueue<Number> dataQ1 = new ConcurrentLinkedQueue<Number>();
	private ConcurrentLinkedQueue<Number> dataQ2 = new ConcurrentLinkedQueue<Number>();
	private ConcurrentLinkedQueue<Number> dataQ3 = new ConcurrentLinkedQueue<Number>();

	private NumberAxis xAxis;

	private void init(Stage primaryStage) {
		xAxis.setForceZeroInRange(false);
		xAxis.setAutoRanging(false);
		xAxis.setTickLabelsVisible(false);
		xAxis.setTickMarkVisible(false);
		xAxis.setMinorTickVisible(false);

		NumberAxis yAxis = new NumberAxis();
		yAxis.setAutoRanging(true);

		// -- Chart
		final LineChart<Number, Number> sc = new LineChart<Number, Number>(xAxis, yAxis) {
			// Override to remove symbols on each data point
			@Override
			protected void dataItemAdded(Series<Number, Number> series, int itemIndex, Data<Number, Number> item) {
			}
		};
		sc.setAnimated(false);
		sc.setId("liveLineeChart");
		sc.setTitle("Animated Line Chart");

		// -- Chart Series

		series2 = new XYChart.Series<Number, Number>();
		series3 = new XYChart.Series<Number, Number>();
		sc.getData().addAll(series2, series3);

		primaryStage.setScene(new Scene(sc));
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Animated Line Chart Sample");
		init(stage);
		stage.show();

		executor = Executors.newCachedThreadPool(new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				Thread thread = new Thread(r);
				thread.setDaemon(true);
				return thread;
			}
		});
		addToQueue = new AddToQueue();
		executor.execute(addToQueue);
		// -- Prepare Timeline
		prepareTimeline();

	}

	private class AddToQueue implements Runnable {
		public void run() {
			try {
				// add a item of random data to queue
				Stock stock = YahooFinance.get("GOOG", true);
				List<HistoricalQuote> listehq;
				listehq = new ArrayList<HistoricalQuote>();
				try {
					listehq = stock.getHistory();

				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("hhdjdjdjjd");
				for (HistoricalQuote historicalQuote : listehq) {
					System.out.println("symbol " + historicalQuote.getSymbol());
					System.out.println("adj close" + historicalQuote.getAdjClose());
					System.out.println("close " + historicalQuote.getClose());
					System.out.println("date " + historicalQuote.getDate().getTime());
					System.out.println("high" + historicalQuote.getHigh());
					System.out.println("low " + historicalQuote.getLow());
					System.out.println("open " + historicalQuote.getOpen());
					System.out.println("volume " + historicalQuote.getVolume());

				}

				dataQ2.add(0);
				dataQ3.add(0);

				Thread.sleep(500);
				executor.execute(this);
			} catch (InterruptedException | IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	// -- Timeline gets called in the JavaFX Main thread
	private void prepareTimeline() {
		// Every frame to take any data from queue and add to chart
		new AnimationTimer() {
			@Override
			public void handle(long now) {
				addDataToSeries();
			}
		}.start();
	}

	private void addDataToSeries() {
		for (int i = 0; i < 20; i++) { // -- add 20 numbers to the plot+
			if (dataQ1.isEmpty())
				break;
			series2.getData().add(new AreaChart.Data(xSeriesData++, dataQ2.remove()));
			series3.getData().add(new AreaChart.Data(xSeriesData++, dataQ3.remove()));
		}
		// remove points to keep us at no more than MAX_DATA_POINTS

		if (series2.getData().size() > MAX_DATA_POINTS) {
			series2.getData().remove(0, series2.getData().size() - MAX_DATA_POINTS);
		}
		if (series3.getData().size() > MAX_DATA_POINTS) {
			series3.getData().remove(0, series3.getData().size() - MAX_DATA_POINTS);
		}
		// update
		xAxis.setLowerBound(xSeriesData - MAX_DATA_POINTS);
		xAxis.setUpperBound(xSeriesData - 1);
	}

	public static void main(String[] args) {
		launch(args);
	}

}
*/