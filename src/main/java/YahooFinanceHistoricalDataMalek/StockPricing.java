package YahooFinanceHistoricalDataMalek;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.Stage;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.jfoenix.controls.JFXTreeTableColumn;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;
import javafx.scene.input.MouseEvent;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;
import javafx.scene.control.TreeTableColumn;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import javafx.beans.value.ObservableValue;
import javafx.util.Callback;
import tn.esprit.Controllers.Client_interface_newController;
import YahooFinanceHistoricalDataMalek.Stocks;
import javafx.scene.control.TreeItem;
import javafx.stage.Stage;
import com.jfoenix.controls.JFXTreeTableView;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedAreaChart;
import javafx.scene.chart.XYChart;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;

import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

public class StockPricing implements Initializable{
	
	
	    List<HistoricalQuote> listehq;
	  
	    @FXML
	    private JFXTreeTableView<Stocks> tableviewstocks1;
	    ObservableList<Stocks> stocks;
    @FXML
    private JFXButton btnHome;

    @FXML
    private AnchorPane holderPane;



    @FXML
    private JFXTextField symbole;

    @FXML
    private JFXButton btngo;

    @FXML
    private Button logout;

    @FXML
    private Button chart;

    @FXML
    private TextField open;

    @FXML
    private TextField low;

    @FXML
    private TextField highh;

    @FXML
    private TextField close;

    @FXML
    private TextField adjclose;

    @FXML
    private Button btncalstock;

    @FXML
    private Label resultat;


    @FXML
    private Button btncalstock1;

    @FXML
    private Label resultat1;

    @FXML
    private Button btncalstock11;

    @FXML
    private Label resultat11;

    @FXML
    private Button btncalstock111;

    @FXML
    private Label resultat111;


	
public void searchforStocksbysymbol(String period , int num){
	Stock stock = null;
	if (period.equals("none") && num==0 )
	 {
		try {
			stock = YahooFinance.get(symbole.getText(),true);
		} catch (IOException ex) {
		
			symbole.requestFocus();
     }}
	else {
		try {
			Calendar from = Calendar.getInstance();
			Calendar to = Calendar.getInstance();
			if(period.equals("Month")){
			from.add(Calendar.MONTH, -num);
			stock= YahooFinance.get(symbole.getText(), from, to, Interval.MONTHLY);}
			else if (period.equals("Year")){
				from.add(Calendar.YEAR, -num);
				stock= YahooFinance.get(symbole.getText(), from, to, Interval.WEEKLY);}
			else if (period.equals("Day")){
				from.add(Calendar.DAY_OF_YEAR, -num);
				stock= YahooFinance.get(symbole.getText(), from, to, Interval.DAILY);}
			
			
		} catch (IOException ex) {
			symbole.requestFocus();
     }
	}
		
		if (stock!=null)
		{System.out.println("currency"+stock.getCurrency());
		System.out.println("name"+stock.getName());
		System.out.println("stock exchange"+stock.getStockExchange());
		System.out.println("symbol "+stock.getSymbol());
		
		
     pupulatetableviewstocks(stock);}
		

}
	 @FXML
	    void btngoclicked(ActionEvent event)  {
           searchforStocksbysymbol("none",0);

	    }

	 
	 private void pupulatetableviewstocks(Stock stock) {
    	 JFXTreeTableColumn<Stocks, String> adjclose = new JFXTreeTableColumn<>("Adj close");
         adjclose.setPrefWidth(150);
        adjclose.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
             @Override
             public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
                 return param.getValue().getValue().adjclose;
             }});
        
        JFXTreeTableColumn<Stocks, String> close = new JFXTreeTableColumn<>("Close");
        close.setPrefWidth(150);
        close.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
             @Override
             public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
                 return param.getValue().getValue().close;
             }});
        
        JFXTreeTableColumn<Stocks, String> date = new JFXTreeTableColumn<>("Date");
        date.setPrefWidth(150);
        date.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
             @Override
             public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
                 return param.getValue().getValue().date;
             }});
        
        JFXTreeTableColumn<Stocks, String> highh = new JFXTreeTableColumn<>("High");
        highh.setPrefWidth(150);
        highh.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
             @Override
             public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
                 return param.getValue().getValue().highh;
             }});
        JFXTreeTableColumn<Stocks, String> low = new JFXTreeTableColumn<>("Low");
        low.setPrefWidth(150);
        low.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
             @Override
             public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
                 return param.getValue().getValue().low;
             }});
        JFXTreeTableColumn<Stocks, String> open = new JFXTreeTableColumn<>("Open");
        open.setPrefWidth(150);
        open.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
             @Override
             public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
                 return param.getValue().getValue().open;
             }});
        JFXTreeTableColumn<Stocks, String> volume = new JFXTreeTableColumn<>("Volume");
        volume.setPrefWidth(150);
        volume.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<Stocks, String>, ObservableValue<String>>() {
             @Override
             public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<Stocks, String> param) {
                 return param.getValue().getValue().volume;
             }});

       stocks = FXCollections.observableArrayList();
     
     listehq=new ArrayList<HistoricalQuote>();
		try {
			listehq = stock.getHistory();
			
		} catch (IOException e) {
		
			e.printStackTrace();
		}
		System.out.println("hhdjdjdjjd");
		for (HistoricalQuote historicalQuote : listehq) {
			System.out.println("symbol "+historicalQuote.getSymbol());
			System.out.println("adj close"+historicalQuote.getAdjClose());
			System.out.println("close " +historicalQuote.getClose());
			System.out.println("date "+historicalQuote.getDate().getTime());
			System.out.println("high"+historicalQuote.getHigh());
			System.out.println("low "+historicalQuote.getLow());
			System.out.println("open "+historicalQuote.getOpen());
			System.out.println("volume "+historicalQuote.getVolume());
			
			
		}
    //    XYChart.Series series = new XYChart.Series<String, Number>();
     //   XYChart.Series series1 = new XYChart.Series<String, Number>();
     //   XYChart.Series series2 = new XYChart.Series<String, Number>();
      //  series.setName(stock.getName());
         for (HistoricalQuote historicalQuote : listehq) {
        	 System.out.println("ttttttttttttttttttttttttttttttttttttttttttt");
            System.out.println(historicalQuote.getClose());
            System.out.println("ttttttttttttttttttttttttttttttttttttttttttt");
            if (historicalQuote.getClose()!=null)
            {SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd");
     
                System.out.println(historicalQuote.getClose());
            	Stocks ltable=new Stocks(historicalQuote.getAdjClose().toString(), historicalQuote.getClose().toString(), historicalQuote.getDate().getTime().toString(), historicalQuote.getHigh().toString(), historicalQuote.getLow().toString(), historicalQuote.getOpen().toString(), historicalQuote.getVolume().toString());
            	stocks.add(ltable);}
         }
        //leaves.add(new Leavess("2012", "kkkk", "hjj", "jjj", "jjjj"));   leaves.add(new Leavess("2012", "kkkk", "hjj", "jjj", "jjjj"));

         final TreeItem<Stocks> root;
         root = new RecursiveTreeItem<Stocks>(stocks, RecursiveTreeObject::getChildren);
         tableviewstocks1.getColumns().setAll(adjclose,close,date,highh,low,open,volume);
         tableviewstocks1.setRoot(root);
         tableviewstocks1.setShowRoot(false);
       
	 }
		public void initialize(URL location, ResourceBundle resources) {
			// TODO Auto-generated method stub
			
		}
	

	    @FXML
	    void logout(ActionEvent event)throws NamingException, IOException {
	    	Stage stage = (Stage) logout.getScene().getWindow();
			stage.close();
	    }

		  
	   @FXML
	    void mouse1(MouseEvent event) {
		  /* if (tableviewstocks1.getSelectionModel().getSelectedItem() != null) {
				TreeItem<Stocks> r = tableviewstocks1.getSelectionModel().getSelectedItem();
			
				adjclose.setText(r.getValue().getAdjclose().getValue().toString() + "");
				close.setText(r.getValue().getClose().getValue().toString() + "");
				highh.setText(r.getValue().getHighh().getValue().toString() + "");
				low.setText(r.getValue().getLow().getValue().toString() + "");
				open.setText(r.getValue().getOpen().getValue().toString() + "");
			}*/
		   adjclose.setText(tableviewstocks1.getSelectionModel().getSelectedItem().getValue().adjclose.getValue().toString());

highh.setText(tableviewstocks1.getSelectionModel().getSelectedItem().getValue().highh.getValue().toString());
close.setText(tableviewstocks1.getSelectionModel().getSelectedItem().getValue().close.getValue().toString());
low.setText(tableviewstocks1.getSelectionModel().getSelectedItem().getValue().low.getValue().toString());
open.setText(tableviewstocks1.getSelectionModel().getSelectedItem().getValue().open.getValue().toString());
	  
	   }

  
	   @FXML
	    void btncalstock(ActionEvent event) {

		 //Average of the Open, High, Low and Close (OHLC Average)
			Float a = Float.parseFloat(adjclose.getText());
			Float b = Float.parseFloat(close.getText());
			Float c = Float.parseFloat(highh.getText());
			Float d = Float.parseFloat(low.getText());
			Float e = Float.parseFloat(open.getText());


			Float prix = (b*c*d*e)/4;
			String pri = Float.toString(prix);
			resultat.setText(pri);
	    }
	   @FXML
	    void btnDSA(ActionEvent event) {
		   // daily stock activity DSA = (High - Low)/Open en %
		 Float a = Float.parseFloat(adjclose.getText());
			Float b = Float.parseFloat(close.getText());
			Float c = Float.parseFloat(highh.getText());
			Float d = Float.parseFloat(low.getText());
			Float e = Float.parseFloat(open.getText());
			Float p=(c-d)*(c-d);
			Float prix =Math.abs(((c-e)*(e-d))/p);
			String pri = Float.toString(prix);
			resultat1.setText(pri);
	    }

	    @FXML
	    void btnGainSell(ActionEvent event) {
				Float d = Float.parseFloat(low.getText());
				Float e = Float.parseFloat(open.getText());
				
				Float prix = e-d;
				String pri = Float.toString(prix);
				resultat111.setText(pri);
	    }

	    @FXML
	    void btncalGainBuy(ActionEvent event) {
				Float c = Float.parseFloat(highh.getText());
				Float e = Float.parseFloat(open.getText());
				
				Float prix = c-e;
				String pri = Float.toString(prix);
				resultat11.setText(pri);
	    }

}
