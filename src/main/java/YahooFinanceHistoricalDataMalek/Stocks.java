package YahooFinanceHistoricalDataMalek;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class Stocks extends RecursiveTreeObject<Stocks> {

	public StringProperty adjclose;
	public StringProperty close;
	public StringProperty date;
	public StringProperty highh;
	public StringProperty low;
	public StringProperty open;
	public StringProperty volume;
	
	 
	public Stocks(String adjclose, String close , String date , String highh, String low, String open, String volume) {
		
		this.adjclose = new SimpleStringProperty(adjclose);
		this.close = new SimpleStringProperty(close);
		this.date = new SimpleStringProperty(date);
		this.highh = new SimpleStringProperty(highh);
		this.low= new SimpleStringProperty(low);
		this.open = new SimpleStringProperty(volume);
		
	}


	public StringProperty getAdjclose() {
		return adjclose;
	}


	public void setAdjclose(StringProperty adjclose) {
		this.adjclose = adjclose;
	}


	public StringProperty getClose() {
		return close;
	}


	public void setClose(StringProperty close) {
		this.close = close;
	}


	public StringProperty getDate() {
		return date;
	}


	public void setDate(StringProperty date) {
		this.date = date;
	}


	public StringProperty getHighh() {
		return highh;
	}


	public void setHighh(StringProperty highh) {
		this.highh = highh;
	}


	public StringProperty getLow() {
		return low;
	}


	public void setLow(StringProperty low) {
		this.low = low;
	}


	public StringProperty getOpen() {
		return open;
	}


	public void setOpen(StringProperty open) {
		this.open = open;
	}


	public StringProperty getVolume() {
		return volume;
	}


	public void setVolume(StringProperty volume) {
		this.volume = volume;
	}
	
	

}